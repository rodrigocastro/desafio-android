package com.castrodev.github_api.repositories;

import com.castrodev.github_api.model.GithubRepository;
import com.castrodev.github_api.model.Item;

import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.Mock;
import org.mockito.runners.MockitoJUnitRunner;

import static junit.framework.Assert.assertEquals;
import static junit.framework.Assert.assertNull;
import static org.mockito.Matchers.anyObject;
import static org.mockito.Mockito.times;
import static org.mockito.Mockito.verify;

/**
 * Created by rodrigocastro on 17/05/17.
 */
@RunWith(MockitoJUnitRunner.class)
public class RepositoriesPresenterTest {

    @Mock
    RepositoriesView view;

    private RepositoriesPresenterImpl presenter;

    @Before
    public void setUp() throws Exception {
        presenter = new RepositoriesPresenterImpl(new FakeRepositoriesInteractorImpl());
        presenter.bindView(view);
    }

    @Test
    public void bindView() throws Exception {
        assertEquals(presenter.getView(), view);
    }

    @Test
    public void unbindView() throws Exception {
        presenter.unbindView();
        assertNull(presenter.getView());
    }

    @Test
    public void onItemClicked() throws Exception {
        presenter.onItemClicked((Item) anyObject());
        verify(view, times(1)).goToPullRequests((Item) anyObject());
    }

    @Test
    public void mockRequestResult() {
        presenter.onFinished((GithubRepository) anyObject());
        verify(view, times(1)).setItems((GithubRepository) anyObject());
        verify(view, times(2)).hideProgress();
    }

}