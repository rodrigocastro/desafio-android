package com.castrodev.github_api.pull_requests;

import com.castrodev.github_api.model.PullRequest;

import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.Mock;
import org.mockito.runners.MockitoJUnitRunner;

import java.util.List;

import static junit.framework.Assert.assertEquals;
import static junit.framework.Assert.assertNull;
import static org.mockito.Matchers.anyObject;
import static org.mockito.Mockito.times;
import static org.mockito.Mockito.verify;

/**
 * Created by rodrigocastro on 17/05/17.
 */
@RunWith(MockitoJUnitRunner.class)
public class PullRequestsPresenterTest {

    public static final String REPOSITORY_NAME = "repository";
    public static final String USER_NAME = "username";
    @Mock
    PullRequestsView view;

    private PullRequestsPresenterImpl presenter;

    @Before
    public void setUp() throws Exception {
        presenter = new PullRequestsPresenterImpl(USER_NAME, REPOSITORY_NAME,new FakePullRequestsInteractorImpl());
        presenter.bindView(view);
    }

    @Test
    public void bindView() throws Exception {
        assertEquals(presenter.getView(), view);
    }

    @Test
    public void unbindView() throws Exception {
        presenter.unbindView();
        assertNull(presenter.getView());
    }

    @Test
    public void onItemClicked() throws Exception {
        presenter.onItemClicked((PullRequest) anyObject());
        verify(view, times(1)).goToPullRequestsSite((PullRequest) anyObject());
    }

    @Test
    public void mockRequestResult() {
        presenter.onFinished((List<PullRequest>) anyObject());
        verify(view, times(1)).setItems((List<PullRequest>) anyObject());
        verify(view, times(2)).hideProgress();
    }

}