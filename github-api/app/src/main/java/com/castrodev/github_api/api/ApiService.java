package com.castrodev.github_api.api;

import com.castrodev.github_api.model.GithubRepository;
import com.castrodev.github_api.model.PullRequest;

import java.util.List;

import retrofit2.Call;
import retrofit2.http.GET;
import retrofit2.http.Path;
import retrofit2.http.Query;

/**
 * Created by rodrigocastro on 17/05/17.
 */

public interface ApiService {

    @GET("search/repositories?q=language:Java&sort=stars")
    Call<GithubRepository> fetchRepositories(@Query("page") String page,
                                             @Query("per_page") String perPage);


    @GET("repos/{creator}/{repository}/pulls")
    Call<List<PullRequest>> fetchPullRequests(@Path("creator") String creator, @Path("repository") String repository);

}
