package com.castrodev.github_api.pull_requests;

import com.castrodev.github_api.model.PullRequest;

/**
 * Created by rodrigocastro on 17/05/17.
 */

public interface PullRequestsPresenter {

    void bindView(PullRequestsView view);
    void unbindView();
    void reload();
    void onItemClicked(PullRequest pullRequest);
}
