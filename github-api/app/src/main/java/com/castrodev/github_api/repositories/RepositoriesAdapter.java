package com.castrodev.github_api.repositories;

import android.content.Context;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import com.castrodev.github_api.R;
import com.castrodev.github_api.model.GithubRepository;
import com.castrodev.github_api.model.Item;
import com.squareup.picasso.Picasso;

import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;

/**
 * Created by rodrigocastro on 16/05/17.
 */

class RepositoriesAdapter extends RecyclerView.Adapter<RepositoriesAdapter.ViewHolder>  {
    private RepositoriesPresenter presenter;
    private List<Item> mDataset;
    private GithubRepository parent;

    RepositoriesAdapter(GithubRepository parent, RepositoriesPresenter presenter) {
        this.parent = parent;
        this.presenter = presenter;
        mDataset = parent.getItems();
    }

    @Override
    public RepositoriesAdapter.ViewHolder onCreateViewHolder(ViewGroup parent,
                                                     int viewType) {
        Context context = parent.getContext();
        LayoutInflater inflater = LayoutInflater.from(context);
        View noteView = inflater.inflate(R.layout.item_repository, parent, false);

        return new ViewHolder(noteView);
    }

    @Override
    public void onBindViewHolder(ViewHolder holder, int position) {
        Item item = mDataset.get(position);
        if (isLast(position)) {
            presenter.paginate(position);
        }
        holder.name.setText(item.getName());
        holder.description.setText(item.getDescription());
        holder.forks.setText(String.valueOf(item.getForks()));
        holder.stars.setText(String.valueOf(item.getStargazersCount()));
        holder.username.setText(item.getOwner().getLogin());

        Context context = holder.thumbnail.getContext();
        Picasso.with(context)
                .load(item.getOwner().getAvatarUrl())
                .error(R.drawable.ic_person)
                .resize(72, 72)
                .into(holder.thumbnail);

    }

    @Override
    public int getItemCount() {
        return mDataset.size();
    }

    private boolean isLast(int position) {
        return position == mDataset.size()-1;
    }

    void concatenateDataSet(GithubRepository repository) {
        mDataset.addAll(repository.getItems());
        parent = repository;
        notifyDataSetChanged();
    }

    class ViewHolder extends RecyclerView.ViewHolder implements View.OnClickListener {
        @BindView(R.id.tv_repository_name)
        TextView name;
        @BindView(R.id.tv_repository_description)
        TextView description;
        @BindView(R.id.tv_repository_forks)
        TextView forks;
        @BindView(R.id.tv_repository_stars)
        TextView stars;
        @BindView(R.id.tv_repository_username)
        TextView username;
        @BindView(R.id.iv_repository_thumb)
        ImageView thumbnail;


        ViewHolder(View itemView) {
            super(itemView);
            ButterKnife.bind(this, itemView);
            itemView.setOnClickListener(this);
        }

        @Override
        public void onClick(View v) {
            int position = getAdapterPosition();
            Item item = mDataset.get(position);
            presenter.onItemClicked(item);
        }
    }
}
