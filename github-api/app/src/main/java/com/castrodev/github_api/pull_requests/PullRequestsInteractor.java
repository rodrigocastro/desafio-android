package com.castrodev.github_api.pull_requests;

import com.castrodev.github_api.model.PullRequest;

import java.util.List;

/**
 * Created by rodrigocastro on 17/05/17.
 */

public interface PullRequestsInteractor {

    public interface OnFinishedListener {
        void onFinished(List<PullRequest> pullRequests);
        void onDefaultError();
    }

    void getPullRequests(String creator, String repository, final OnFinishedListener listener);
}
