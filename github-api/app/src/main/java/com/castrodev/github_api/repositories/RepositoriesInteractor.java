package com.castrodev.github_api.repositories;

import com.castrodev.github_api.model.GithubRepository;

/**
 * Created by rodrigocastro on 15/05/17.
 */

public interface RepositoriesInteractor {

    public interface OnFinishedListener {
        void onFinished(GithubRepository repository);
        void onDefaultError();
    }

    void getRepositories(int page, int perPage, final OnFinishedListener listener);
}
