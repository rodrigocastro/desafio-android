package com.castrodev.github_api.repositories;

import com.castrodev.github_api.model.Item;

/**
 * Created by rodrigocastro on 15/05/17.
 */

public interface RepositoriesPresenter {

    void bindView(RepositoriesView view);
    void unbindView();
    void reload();
    void onItemClicked(Item item);
    void paginate(int page);
}
