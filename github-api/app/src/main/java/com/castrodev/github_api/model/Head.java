
package com.castrodev.github_api.model;

import com.google.gson.annotations.SerializedName;

import javax.annotation.Generated;

@Generated("net.hexar.json2pojo")
public class Head {

    @SerializedName("label")
    private String mLabel;
    @SerializedName("ref")
    private String mRef;
    @SerializedName("repo")
    private Item repo;
    @SerializedName("sha")
    private String mSha;
    @SerializedName("user")
    private User mUser;

    public String getLabel() {
        return mLabel;
    }

    public void setLabel(String label) {
        mLabel = label;
    }

    public String getRef() {
        return mRef;
    }

    public void setRef(String ref) {
        mRef = ref;
    }

    public Item getRepo() {
        return repo;
    }

    public void setRepo(Item repo) {
        repo = repo;
    }

    public String getSha() {
        return mSha;
    }

    public void setSha(String sha) {
        mSha = sha;
    }

    public User getUser() {
        return mUser;
    }

    public void setUser(User user) {
        mUser = user;
    }

}
