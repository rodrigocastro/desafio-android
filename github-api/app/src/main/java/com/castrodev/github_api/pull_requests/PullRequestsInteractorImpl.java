package com.castrodev.github_api.pull_requests;

import com.castrodev.github_api.api.ApiClient;
import com.castrodev.github_api.api.ApiService;
import com.castrodev.github_api.model.PullRequest;

import java.util.List;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

/**
 * Created by rodrigocastro on 17/05/17.
 */

class PullRequestsInteractorImpl implements PullRequestsInteractor {
    @Override
    public void getPullRequests(String creator, String repository, final OnFinishedListener listener) {
        ApiService apiService =
                ApiClient.getClient().create(ApiService.class);

        Call<List<PullRequest>> call = apiService.fetchPullRequests(creator, repository);
        call.enqueue(new Callback<List<PullRequest>>() {
            @Override
            public void onResponse(Call<List<PullRequest>> call, Response<List<PullRequest>> response) {
                listener.onFinished(response.body());
            }

            @Override
            public void onFailure(Call<List<PullRequest>> call, Throwable t) {
                listener.onDefaultError();
            }
        });
    }
}
