package com.castrodev.github_api.pull_requests;

import android.content.Context;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import com.castrodev.github_api.R;
import com.castrodev.github_api.model.PullRequest;
import com.squareup.picasso.Picasso;

import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.List;
import java.util.Locale;

import butterknife.BindView;
import butterknife.ButterKnife;

/**
 * Created by rodrigocastro on 16/05/17.
 */

class PullRequestsAdapter extends RecyclerView.Adapter<PullRequestsAdapter.ViewHolder> {

    private PullRequestsPresenter presenter;
    private List<PullRequest> mDataset;

    PullRequestsAdapter(List<PullRequest> pullRequests, PullRequestsPresenter presenter) {
        this.presenter = presenter;
        mDataset = pullRequests;
    }

    @Override
    public PullRequestsAdapter.ViewHolder onCreateViewHolder(ViewGroup parent,
                                                             int viewType) {
        Context context = parent.getContext();
        LayoutInflater inflater = LayoutInflater.from(context);
        View noteView = inflater.inflate(R.layout.item_pull_request, parent, false);

        return new ViewHolder(noteView);
    }

    @Override
    public void onBindViewHolder(ViewHolder holder, int position) {
        PullRequest pullRequest = mDataset.get(position);

        holder.title.setText(pullRequest.getTitle());
        holder.description.setText(pullRequest.getBody());
        holder.username.setText(pullRequest.getUser().getLogin());
        holder.createdAt.setText(getFormattedDate(pullRequest.getCreatedAt()));

        Context context = holder.thumbnail.getContext();
        Picasso.with(context)
                .load(pullRequest.getUser().getAvatarUrl())
                .error(R.drawable.ic_person)
                .resize(80, 80)
                .into(holder.thumbnail);

    }

    private String getFormattedDate(Date createdAt) {
        String pattern = "dd/MM/yyyy";
        SimpleDateFormat formatter = new SimpleDateFormat(pattern, Locale.getDefault());
        return formatter.format(createdAt);
    }

    @Override
    public int getItemCount() {
        return mDataset.size();
    }

    class ViewHolder extends RecyclerView.ViewHolder implements View.OnClickListener {
        @BindView(R.id.tv_pull_request_title)
        TextView title;
        @BindView(R.id.tv_pull_request_description)
        TextView description;
        @BindView(R.id.tv_pull_request_username)
        TextView username;
        @BindView(R.id.tv_pull_request_created_at)
        TextView createdAt;
        @BindView(R.id.iv_pull_request_thumbnail)
        ImageView thumbnail;

        ViewHolder(View itemView) {
            super(itemView);
            ButterKnife.bind(this, itemView);
            itemView.setOnClickListener(this);
        }

        @Override
        public void onClick(View v) {
            int position = getAdapterPosition();
            PullRequest pullRequest = mDataset.get(position);
            presenter.onItemClicked(pullRequest);
        }
    }
}
