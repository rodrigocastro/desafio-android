package com.castrodev.github_api.pull_requests;

import android.content.Context;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.net.Uri;
import android.os.Bundle;
import android.support.customtabs.CustomTabsIntent;
import android.support.v4.content.ContextCompat;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.DividerItemDecoration;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.Toolbar;
import android.view.View;
import android.widget.ProgressBar;
import android.widget.TextView;

import com.castrodev.github_api.R;
import com.castrodev.github_api.model.PullRequest;

import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;

import static com.castrodev.github_api.repositories.RepositoriesActivity.REPOSITORY_NAME_KEY;
import static com.castrodev.github_api.repositories.RepositoriesActivity.USER_NAME_KEY;

/**
 * Created by rodrigocastro on 17/05/17.
 */

public class PullRequestsActivity extends AppCompatActivity implements PullRequestsView {

    @BindView(R.id.toolbar)
    Toolbar toolbar;
    @BindView(R.id.recycler_view)
    RecyclerView rvPullRequests;
    @BindView(R.id.tv_error)
    TextView tvError;
    @BindView(R.id.progress)
    ProgressBar progressBar;
    @BindView(R.id.error_view)
    View errorView;

    private PullRequestsPresenterImpl presenter;
    private String repositoryName, userName;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.list_activity);
        ButterKnife.bind(this);

        getExtraValues();
        presenter = new PullRequestsPresenterImpl(userName, repositoryName, new PullRequestsInteractorImpl());
        setupView();
    }

    private void getExtraValues() {
        if (getIntent().hasExtra(REPOSITORY_NAME_KEY)) {
            repositoryName = getIntent().getStringExtra(REPOSITORY_NAME_KEY);
        }
        if (getIntent().hasExtra(USER_NAME_KEY)) {
            userName = getIntent().getStringExtra(USER_NAME_KEY);
        }
    }

    private void setupView() {
        toolbar.setTitle(repositoryName);
        setSupportActionBar(toolbar);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
    }

    @Override
    protected void onResume() {
        super.onResume();
        presenter.bindView(this);
    }

    @Override
    protected void onDestroy() {
        presenter.unbindView();
        super.onDestroy();
    }

    @Override
    public void showProgress() {
        handleProgressVisibility(true);
    }

    @Override
    public void hideProgress() {
        handleProgressVisibility(false);
    }

    private void handleProgressVisibility(boolean visible) {
        progressBar.setVisibility(visible ? View.VISIBLE : View.GONE);
        rvPullRequests.setVisibility(visible ? View.GONE : View.VISIBLE);
    }

    @Override
    public void setItems(List<PullRequest> pullRequests) {
        errorView.setVisibility(View.GONE);
        PullRequestsAdapter adapter = (PullRequestsAdapter) rvPullRequests.getAdapter();
        LinearLayoutManager layoutManager = new LinearLayoutManager(this);
        rvPullRequests.setLayoutManager(layoutManager);
        DividerItemDecoration dividerItemDecoration = new DividerItemDecoration(rvPullRequests.getContext(),
                layoutManager.getOrientation());
        rvPullRequests.addItemDecoration(dividerItemDecoration);
        rvPullRequests.setAdapter(new PullRequestsAdapter(pullRequests, presenter));
    }

    @Override
    public void goToPullRequestsSite(PullRequest pullRequest) {
        int colorInt = ContextCompat.getColor(this, R.color.colorPrimary);
        CustomTabsIntent.Builder builder = new CustomTabsIntent.Builder();
        builder.setToolbarColor(colorInt);
        CustomTabsIntent customTabsIntent = builder.build();
        customTabsIntent.launchUrl(this, Uri.parse(pullRequest.getHtmlUrl()));
    }

    @Override
    public void showDefaultError() {
        showErrorMessage(R.string.unexpected_error);
    }

    @Override
    public void showNetworkError() {
        showErrorMessage(R.string.internet_error);
    }

    private void showErrorMessage(int message) {
        rvPullRequests.setVisibility(View.GONE);
        errorView.setVisibility(View.VISIBLE);
        tvError.setText(message);
    }

    @Override
    public boolean isConnected() {
        ConnectivityManager cm =
                (ConnectivityManager) getSystemService(Context.CONNECTIVITY_SERVICE);
        NetworkInfo netInfo = cm.getActiveNetworkInfo();
        return netInfo != null && netInfo.isConnectedOrConnecting();
    }

    @OnClick(R.id.bt_try_again)
    public void tryAgain(View v) {
        presenter.reload();
    }
}
