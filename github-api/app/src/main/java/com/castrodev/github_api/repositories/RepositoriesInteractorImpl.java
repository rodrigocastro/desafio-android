package com.castrodev.github_api.repositories;

import com.castrodev.github_api.api.ApiClient;
import com.castrodev.github_api.api.ApiService;
import com.castrodev.github_api.model.GithubRepository;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

/**
 * Created by rodrigocastro on 17/05/17.
 */

class RepositoriesInteractorImpl implements RepositoriesInteractor {

    @Override
    public void getRepositories(int page, int perPage, final OnFinishedListener listener) {
        ApiService apiService =
                ApiClient.getClient().create(ApiService.class);

        Call<GithubRepository> call = apiService.fetchRepositories(String.valueOf(page), String.valueOf(perPage));
        call.enqueue(new Callback<GithubRepository>() {
            @Override
            public void onResponse(Call<GithubRepository> call, Response<GithubRepository> response) {
                listener.onFinished(response.body());
            }

            @Override
            public void onFailure(Call<GithubRepository> call, Throwable t) {
                listener.onDefaultError();
            }
        });
    }
}
