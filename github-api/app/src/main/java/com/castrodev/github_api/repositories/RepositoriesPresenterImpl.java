package com.castrodev.github_api.repositories;

import com.castrodev.github_api.model.GithubRepository;
import com.castrodev.github_api.model.Item;

/**
 * Created by rodrigocastro on 16/05/17.
 */

public class RepositoriesPresenterImpl implements RepositoriesPresenter, RepositoriesInteractor.OnFinishedListener {

    private RepositoriesView repositoriesView;
    private RepositoriesInteractor repositoriesInteractor;

    public RepositoriesPresenterImpl(RepositoriesInteractor repositoriesInteractor) {
        this.repositoriesInteractor = repositoriesInteractor;
    }

    @Override
    public void bindView(RepositoriesView view) {
        if (view != null) {
            repositoriesView = view;
            repositoriesView.showProgress();

            if (!repositoriesView.isConnected()) {
                repositoriesView.showNetworkError();
                repositoriesView.hideProgress();
                return;
            }
        }

        repositoriesInteractor.getRepositories(1, 10, this);
    }

    @Override
    public void unbindView() {
        repositoriesView = null;
    }

    @Override
    public void reload() {
        if (repositoriesView != null) {
            repositoriesView.showProgress();
            repositoriesInteractor.getRepositories(1, 10, this);
        }
    }

    @Override
    public void onItemClicked(Item item) {
        if (repositoriesView != null) {
            repositoriesView.goToPullRequests(item);
        }
    }

    @Override
    public void paginate(int page) {
        repositoriesInteractor.getRepositories(page, 10, this);
    }

    @Override
    public void onFinished(GithubRepository repository) {
        if (repositoriesView != null) {
            repositoriesView.setItems(repository);
            repositoriesView.hideProgress();
        }
    }

    @Override
    public void onDefaultError() {
        if (repositoriesView != null) {
            repositoriesView.showDefaultError();
            repositoriesView.hideProgress();
        }
    }

    public RepositoriesView getView(){
        return repositoriesView;
    }
}
