package com.castrodev.github_api.pull_requests;

import com.castrodev.github_api.model.PullRequest;
import com.google.gson.Gson;
import com.google.gson.reflect.TypeToken;

import java.lang.reflect.Type;
import java.util.List;

/**
 * Created by rodrigocastro on 17/05/17.
 */

class FakePullRequestsInteractorImpl implements PullRequestsInteractor {
    @Override
    public void getPullRequests(String creator, String repository, OnFinishedListener listener) {
        Gson gson = new Gson();
        Type listType = new TypeToken<List<PullRequest>>(){}.getType();
        listener.onFinished((List<PullRequest>) gson.fromJson(mockJson, listType));
    }

    String mockJson = "[\n" +
            "  {\n" +
            "    \"url\": \"https://api.github.com/repos/square/retrofit/pulls/2336\",\n" +
            "    \"id\": 120963863,\n" +
            "    \"html_url\": \"https://github.com/square/retrofit/pull/2336\",\n" +
            "    \"diff_url\": \"https://github.com/square/retrofit/pull/2336.diff\",\n" +
            "    \"patch_url\": \"https://github.com/square/retrofit/pull/2336.patch\",\n" +
            "    \"issue_url\": \"https://api.github.com/repos/square/retrofit/issues/2336\",\n" +
            "    \"number\": 2336,\n" +
            "    \"state\": \"open\",\n" +
            "    \"locked\": false,\n" +
            "    \"title\": \"Add ProGuard rules\",\n" +
            "    \"user\": {\n" +
            "      \"login\": \"PromanSEW\",\n" +
            "      \"id\": 5219866,\n" +
            "      \"avatar_url\": \"https://avatars1.githubusercontent.com/u/5219866?v=3\",\n" +
            "      \"gravatar_id\": \"\",\n" +
            "      \"url\": \"https://api.github.com/users/PromanSEW\",\n" +
            "      \"html_url\": \"https://github.com/PromanSEW\",\n" +
            "      \"followers_url\": \"https://api.github.com/users/PromanSEW/followers\",\n" +
            "      \"following_url\": \"https://api.github.com/users/PromanSEW/following{/other_user}\",\n" +
            "      \"gists_url\": \"https://api.github.com/users/PromanSEW/gists{/gist_id}\",\n" +
            "      \"starred_url\": \"https://api.github.com/users/PromanSEW/starred{/owner}{/repo}\",\n" +
            "      \"subscriptions_url\": \"https://api.github.com/users/PromanSEW/subscriptions\",\n" +
            "      \"organizations_url\": \"https://api.github.com/users/PromanSEW/orgs\",\n" +
            "      \"repos_url\": \"https://api.github.com/users/PromanSEW/repos\",\n" +
            "      \"events_url\": \"https://api.github.com/users/PromanSEW/events{/privacy}\",\n" +
            "      \"received_events_url\": \"https://api.github.com/users/PromanSEW/received_events\",\n" +
            "      \"type\": \"User\",\n" +
            "      \"site_admin\": false\n" +
            "    },\n" +
            "    \"body\": \"These rules were copied from OkHttp README. If there are Retrofit specific settings, please note.\\r\\nAlso see #2335 for additional information\",\n" +
            "    \"created_at\": \"2017-05-17T02:36:30Z\",\n" +
            "    \"updated_at\": \"2017-05-17T02:38:36Z\",\n" +
            "    \"closed_at\": null,\n" +
            "    \"merged_at\": null,\n" +
            "    \"merge_commit_sha\": \"827135af1cd9dd2bffcbd79c2cd2f93dc0106202\",\n" +
            "    \"assignee\": null,\n" +
            "    \"assignees\": [],\n" +
            "    \"requested_reviewers\": [],\n" +
            "    \"milestone\": null,\n" +
            "    \"commits_url\": \"https://api.github.com/repos/square/retrofit/pulls/2336/commits\",\n" +
            "    \"review_comments_url\": \"https://api.github.com/repos/square/retrofit/pulls/2336/comments\",\n" +
            "    \"review_comment_url\": \"https://api.github.com/repos/square/retrofit/pulls/comments{/number}\",\n" +
            "    \"comments_url\": \"https://api.github.com/repos/square/retrofit/issues/2336/comments\",\n" +
            "    \"statuses_url\": \"https://api.github.com/repos/square/retrofit/statuses/b542a834cba090a931a46746e7eb010ab5e71a6d\",\n" +
            "    \"head\": {\n" +
            "      \"label\": \"PromanSEW:patch-1\",\n" +
            "      \"ref\": \"patch-1\",\n" +
            "      \"sha\": \"b542a834cba090a931a46746e7eb010ab5e71a6d\",\n" +
            "      \"user\": {\n" +
            "        \"login\": \"PromanSEW\",\n" +
            "        \"id\": 5219866,\n" +
            "        \"avatar_url\": \"https://avatars1.githubusercontent.com/u/5219866?v=3\",\n" +
            "        \"gravatar_id\": \"\",\n" +
            "        \"url\": \"https://api.github.com/users/PromanSEW\",\n" +
            "        \"html_url\": \"https://github.com/PromanSEW\",\n" +
            "        \"followers_url\": \"https://api.github.com/users/PromanSEW/followers\",\n" +
            "        \"following_url\": \"https://api.github.com/users/PromanSEW/following{/other_user}\",\n" +
            "        \"gists_url\": \"https://api.github.com/users/PromanSEW/gists{/gist_id}\",\n" +
            "        \"starred_url\": \"https://api.github.com/users/PromanSEW/starred{/owner}{/repo}\",\n" +
            "        \"subscriptions_url\": \"https://api.github.com/users/PromanSEW/subscriptions\",\n" +
            "        \"organizations_url\": \"https://api.github.com/users/PromanSEW/orgs\",\n" +
            "        \"repos_url\": \"https://api.github.com/users/PromanSEW/repos\",\n" +
            "        \"events_url\": \"https://api.github.com/users/PromanSEW/events{/privacy}\",\n" +
            "        \"received_events_url\": \"https://api.github.com/users/PromanSEW/received_events\",\n" +
            "        \"type\": \"User\",\n" +
            "        \"site_admin\": false\n" +
            "      },\n" +
            "      \"repo\": {\n" +
            "        \"id\": 91217941,\n" +
            "        \"name\": \"retrofit\",\n" +
            "        \"full_name\": \"PromanSEW/retrofit\",\n" +
            "        \"owner\": {\n" +
            "          \"login\": \"PromanSEW\",\n" +
            "          \"id\": 5219866,\n" +
            "          \"avatar_url\": \"https://avatars1.githubusercontent.com/u/5219866?v=3\",\n" +
            "          \"gravatar_id\": \"\",\n" +
            "          \"url\": \"https://api.github.com/users/PromanSEW\",\n" +
            "          \"html_url\": \"https://github.com/PromanSEW\",\n" +
            "          \"followers_url\": \"https://api.github.com/users/PromanSEW/followers\",\n" +
            "          \"following_url\": \"https://api.github.com/users/PromanSEW/following{/other_user}\",\n" +
            "          \"gists_url\": \"https://api.github.com/users/PromanSEW/gists{/gist_id}\",\n" +
            "          \"starred_url\": \"https://api.github.com/users/PromanSEW/starred{/owner}{/repo}\",\n" +
            "          \"subscriptions_url\": \"https://api.github.com/users/PromanSEW/subscriptions\",\n" +
            "          \"organizations_url\": \"https://api.github.com/users/PromanSEW/orgs\",\n" +
            "          \"repos_url\": \"https://api.github.com/users/PromanSEW/repos\",\n" +
            "          \"events_url\": \"https://api.github.com/users/PromanSEW/events{/privacy}\",\n" +
            "          \"received_events_url\": \"https://api.github.com/users/PromanSEW/received_events\",\n" +
            "          \"type\": \"User\",\n" +
            "          \"site_admin\": false\n" +
            "        },\n" +
            "        \"private\": false,\n" +
            "        \"html_url\": \"https://github.com/PromanSEW/retrofit\",\n" +
            "        \"description\": \"Type-safe HTTP client for Android and Java by Square, Inc.\",\n" +
            "        \"fork\": true,\n" +
            "        \"url\": \"https://api.github.com/repos/PromanSEW/retrofit\",\n" +
            "        \"forks_url\": \"https://api.github.com/repos/PromanSEW/retrofit/forks\",\n" +
            "        \"keys_url\": \"https://api.github.com/repos/PromanSEW/retrofit/keys{/key_id}\",\n" +
            "        \"collaborators_url\": \"https://api.github.com/repos/PromanSEW/retrofit/collaborators{/collaborator}\",\n" +
            "        \"teams_url\": \"https://api.github.com/repos/PromanSEW/retrofit/teams\",\n" +
            "        \"hooks_url\": \"https://api.github.com/repos/PromanSEW/retrofit/hooks\",\n" +
            "        \"issue_events_url\": \"https://api.github.com/repos/PromanSEW/retrofit/issues/events{/number}\",\n" +
            "        \"events_url\": \"https://api.github.com/repos/PromanSEW/retrofit/events\",\n" +
            "        \"assignees_url\": \"https://api.github.com/repos/PromanSEW/retrofit/assignees{/user}\",\n" +
            "        \"branches_url\": \"https://api.github.com/repos/PromanSEW/retrofit/branches{/branch}\",\n" +
            "        \"tags_url\": \"https://api.github.com/repos/PromanSEW/retrofit/tags\",\n" +
            "        \"blobs_url\": \"https://api.github.com/repos/PromanSEW/retrofit/git/blobs{/sha}\",\n" +
            "        \"git_tags_url\": \"https://api.github.com/repos/PromanSEW/retrofit/git/tags{/sha}\",\n" +
            "        \"git_refs_url\": \"https://api.github.com/repos/PromanSEW/retrofit/git/refs{/sha}\",\n" +
            "        \"trees_url\": \"https://api.github.com/repos/PromanSEW/retrofit/git/trees{/sha}\",\n" +
            "        \"statuses_url\": \"https://api.github.com/repos/PromanSEW/retrofit/statuses/{sha}\",\n" +
            "        \"languages_url\": \"https://api.github.com/repos/PromanSEW/retrofit/languages\",\n" +
            "        \"stargazers_url\": \"https://api.github.com/repos/PromanSEW/retrofit/stargazers\",\n" +
            "        \"contributors_url\": \"https://api.github.com/repos/PromanSEW/retrofit/contributors\",\n" +
            "        \"subscribers_url\": \"https://api.github.com/repos/PromanSEW/retrofit/subscribers\",\n" +
            "        \"subscription_url\": \"https://api.github.com/repos/PromanSEW/retrofit/subscription\",\n" +
            "        \"commits_url\": \"https://api.github.com/repos/PromanSEW/retrofit/commits{/sha}\",\n" +
            "        \"git_commits_url\": \"https://api.github.com/repos/PromanSEW/retrofit/git/commits{/sha}\",\n" +
            "        \"comments_url\": \"https://api.github.com/repos/PromanSEW/retrofit/comments{/number}\",\n" +
            "        \"issue_comment_url\": \"https://api.github.com/repos/PromanSEW/retrofit/issues/comments{/number}\",\n" +
            "        \"contents_url\": \"https://api.github.com/repos/PromanSEW/retrofit/contents/{+path}\",\n" +
            "        \"compare_url\": \"https://api.github.com/repos/PromanSEW/retrofit/compare/{base}...{head}\",\n" +
            "        \"merges_url\": \"https://api.github.com/repos/PromanSEW/retrofit/merges\",\n" +
            "        \"archive_url\": \"https://api.github.com/repos/PromanSEW/retrofit/{archive_format}{/ref}\",\n" +
            "        \"downloads_url\": \"https://api.github.com/repos/PromanSEW/retrofit/downloads\",\n" +
            "        \"issues_url\": \"https://api.github.com/repos/PromanSEW/retrofit/issues{/number}\",\n" +
            "        \"pulls_url\": \"https://api.github.com/repos/PromanSEW/retrofit/pulls{/number}\",\n" +
            "        \"milestones_url\": \"https://api.github.com/repos/PromanSEW/retrofit/milestones{/number}\",\n" +
            "        \"notifications_url\": \"https://api.github.com/repos/PromanSEW/retrofit/notifications{?since,all,participating}\",\n" +
            "        \"labels_url\": \"https://api.github.com/repos/PromanSEW/retrofit/labels{/name}\",\n" +
            "        \"releases_url\": \"https://api.github.com/repos/PromanSEW/retrofit/releases{/id}\",\n" +
            "        \"deployments_url\": \"https://api.github.com/repos/PromanSEW/retrofit/deployments\",\n" +
            "        \"created_at\": \"2017-05-14T03:40:31Z\",\n" +
            "        \"updated_at\": \"2017-05-14T03:40:34Z\",\n" +
            "        \"pushed_at\": \"2017-05-17T02:27:48Z\",\n" +
            "        \"git_url\": \"git://github.com/PromanSEW/retrofit.git\",\n" +
            "        \"ssh_url\": \"git@github.com:PromanSEW/retrofit.git\",\n" +
            "        \"clone_url\": \"https://github.com/PromanSEW/retrofit.git\",\n" +
            "        \"svn_url\": \"https://github.com/PromanSEW/retrofit\",\n" +
            "        \"homepage\": \"http://square.github.io/retrofit/\",\n" +
            "        \"size\": 4424,\n" +
            "        \"stargazers_count\": 0,\n" +
            "        \"watchers_count\": 0,\n" +
            "        \"language\": \"Java\",\n" +
            "        \"has_issues\": false,\n" +
            "        \"has_projects\": true,\n" +
            "        \"has_downloads\": true,\n" +
            "        \"has_wiki\": true,\n" +
            "        \"has_pages\": false,\n" +
            "        \"forks_count\": 0,\n" +
            "        \"mirror_url\": null,\n" +
            "        \"open_issues_count\": 0,\n" +
            "        \"forks\": 0,\n" +
            "        \"open_issues\": 0,\n" +
            "        \"watchers\": 0,\n" +
            "        \"default_branch\": \"master\"\n" +
            "      }\n" +
            "    },\n" +
            "    \"base\": {\n" +
            "      \"label\": \"square:master\",\n" +
            "      \"ref\": \"master\",\n" +
            "      \"sha\": \"6a30d8e5a81577fc1befcf03c831d9561cdd55d7\",\n" +
            "      \"user\": {\n" +
            "        \"login\": \"square\",\n" +
            "        \"id\": 82592,\n" +
            "        \"avatar_url\": \"https://avatars3.githubusercontent.com/u/82592?v=3\",\n" +
            "        \"gravatar_id\": \"\",\n" +
            "        \"url\": \"https://api.github.com/users/square\",\n" +
            "        \"html_url\": \"https://github.com/square\",\n" +
            "        \"followers_url\": \"https://api.github.com/users/square/followers\",\n" +
            "        \"following_url\": \"https://api.github.com/users/square/following{/other_user}\",\n" +
            "        \"gists_url\": \"https://api.github.com/users/square/gists{/gist_id}\",\n" +
            "        \"starred_url\": \"https://api.github.com/users/square/starred{/owner}{/repo}\",\n" +
            "        \"subscriptions_url\": \"https://api.github.com/users/square/subscriptions\",\n" +
            "        \"organizations_url\": \"https://api.github.com/users/square/orgs\",\n" +
            "        \"repos_url\": \"https://api.github.com/users/square/repos\",\n" +
            "        \"events_url\": \"https://api.github.com/users/square/events{/privacy}\",\n" +
            "        \"received_events_url\": \"https://api.github.com/users/square/received_events\",\n" +
            "        \"type\": \"Organization\",\n" +
            "        \"site_admin\": false\n" +
            "      },\n" +
            "      \"repo\": {\n" +
            "        \"id\": 892275,\n" +
            "        \"name\": \"retrofit\",\n" +
            "        \"full_name\": \"square/retrofit\",\n" +
            "        \"owner\": {\n" +
            "          \"login\": \"square\",\n" +
            "          \"id\": 82592,\n" +
            "          \"avatar_url\": \"https://avatars3.githubusercontent.com/u/82592?v=3\",\n" +
            "          \"gravatar_id\": \"\",\n" +
            "          \"url\": \"https://api.github.com/users/square\",\n" +
            "          \"html_url\": \"https://github.com/square\",\n" +
            "          \"followers_url\": \"https://api.github.com/users/square/followers\",\n" +
            "          \"following_url\": \"https://api.github.com/users/square/following{/other_user}\",\n" +
            "          \"gists_url\": \"https://api.github.com/users/square/gists{/gist_id}\",\n" +
            "          \"starred_url\": \"https://api.github.com/users/square/starred{/owner}{/repo}\",\n" +
            "          \"subscriptions_url\": \"https://api.github.com/users/square/subscriptions\",\n" +
            "          \"organizations_url\": \"https://api.github.com/users/square/orgs\",\n" +
            "          \"repos_url\": \"https://api.github.com/users/square/repos\",\n" +
            "          \"events_url\": \"https://api.github.com/users/square/events{/privacy}\",\n" +
            "          \"received_events_url\": \"https://api.github.com/users/square/received_events\",\n" +
            "          \"type\": \"Organization\",\n" +
            "          \"site_admin\": false\n" +
            "        },\n" +
            "        \"private\": false,\n" +
            "        \"html_url\": \"https://github.com/square/retrofit\",\n" +
            "        \"description\": \"Type-safe HTTP client for Android and Java by Square, Inc.\",\n" +
            "        \"fork\": false,\n" +
            "        \"url\": \"https://api.github.com/repos/square/retrofit\",\n" +
            "        \"forks_url\": \"https://api.github.com/repos/square/retrofit/forks\",\n" +
            "        \"keys_url\": \"https://api.github.com/repos/square/retrofit/keys{/key_id}\",\n" +
            "        \"collaborators_url\": \"https://api.github.com/repos/square/retrofit/collaborators{/collaborator}\",\n" +
            "        \"teams_url\": \"https://api.github.com/repos/square/retrofit/teams\",\n" +
            "        \"hooks_url\": \"https://api.github.com/repos/square/retrofit/hooks\",\n" +
            "        \"issue_events_url\": \"https://api.github.com/repos/square/retrofit/issues/events{/number}\",\n" +
            "        \"events_url\": \"https://api.github.com/repos/square/retrofit/events\",\n" +
            "        \"assignees_url\": \"https://api.github.com/repos/square/retrofit/assignees{/user}\",\n" +
            "        \"branches_url\": \"https://api.github.com/repos/square/retrofit/branches{/branch}\",\n" +
            "        \"tags_url\": \"https://api.github.com/repos/square/retrofit/tags\",\n" +
            "        \"blobs_url\": \"https://api.github.com/repos/square/retrofit/git/blobs{/sha}\",\n" +
            "        \"git_tags_url\": \"https://api.github.com/repos/square/retrofit/git/tags{/sha}\",\n" +
            "        \"git_refs_url\": \"https://api.github.com/repos/square/retrofit/git/refs{/sha}\",\n" +
            "        \"trees_url\": \"https://api.github.com/repos/square/retrofit/git/trees{/sha}\",\n" +
            "        \"statuses_url\": \"https://api.github.com/repos/square/retrofit/statuses/{sha}\",\n" +
            "        \"languages_url\": \"https://api.github.com/repos/square/retrofit/languages\",\n" +
            "        \"stargazers_url\": \"https://api.github.com/repos/square/retrofit/stargazers\",\n" +
            "        \"contributors_url\": \"https://api.github.com/repos/square/retrofit/contributors\",\n" +
            "        \"subscribers_url\": \"https://api.github.com/repos/square/retrofit/subscribers\",\n" +
            "        \"subscription_url\": \"https://api.github.com/repos/square/retrofit/subscription\",\n" +
            "        \"commits_url\": \"https://api.github.com/repos/square/retrofit/commits{/sha}\",\n" +
            "        \"git_commits_url\": \"https://api.github.com/repos/square/retrofit/git/commits{/sha}\",\n" +
            "        \"comments_url\": \"https://api.github.com/repos/square/retrofit/comments{/number}\",\n" +
            "        \"issue_comment_url\": \"https://api.github.com/repos/square/retrofit/issues/comments{/number}\",\n" +
            "        \"contents_url\": \"https://api.github.com/repos/square/retrofit/contents/{+path}\",\n" +
            "        \"compare_url\": \"https://api.github.com/repos/square/retrofit/compare/{base}...{head}\",\n" +
            "        \"merges_url\": \"https://api.github.com/repos/square/retrofit/merges\",\n" +
            "        \"archive_url\": \"https://api.github.com/repos/square/retrofit/{archive_format}{/ref}\",\n" +
            "        \"downloads_url\": \"https://api.github.com/repos/square/retrofit/downloads\",\n" +
            "        \"issues_url\": \"https://api.github.com/repos/square/retrofit/issues{/number}\",\n" +
            "        \"pulls_url\": \"https://api.github.com/repos/square/retrofit/pulls{/number}\",\n" +
            "        \"milestones_url\": \"https://api.github.com/repos/square/retrofit/milestones{/number}\",\n" +
            "        \"notifications_url\": \"https://api.github.com/repos/square/retrofit/notifications{?since,all,participating}\",\n" +
            "        \"labels_url\": \"https://api.github.com/repos/square/retrofit/labels{/name}\",\n" +
            "        \"releases_url\": \"https://api.github.com/repos/square/retrofit/releases{/id}\",\n" +
            "        \"deployments_url\": \"https://api.github.com/repos/square/retrofit/deployments\",\n" +
            "        \"created_at\": \"2010-09-06T21:39:43Z\",\n" +
            "        \"updated_at\": \"2017-05-17T02:40:58Z\",\n" +
            "        \"pushed_at\": \"2017-05-17T02:36:31Z\",\n" +
            "        \"git_url\": \"git://github.com/square/retrofit.git\",\n" +
            "        \"ssh_url\": \"git@github.com:square/retrofit.git\",\n" +
            "        \"clone_url\": \"https://github.com/square/retrofit.git\",\n" +
            "        \"svn_url\": \"https://github.com/square/retrofit\",\n" +
            "        \"homepage\": \"http://square.github.io/retrofit/\",\n" +
            "        \"size\": 4435,\n" +
            "        \"stargazers_count\": 21183,\n" +
            "        \"watchers_count\": 21183,\n" +
            "        \"language\": \"Java\",\n" +
            "        \"has_issues\": true,\n" +
            "        \"has_projects\": true,\n" +
            "        \"has_downloads\": true,\n" +
            "        \"has_wiki\": true,\n" +
            "        \"has_pages\": true,\n" +
            "        \"forks_count\": 4368,\n" +
            "        \"mirror_url\": null,\n" +
            "        \"open_issues_count\": 56,\n" +
            "        \"forks\": 4368,\n" +
            "        \"open_issues\": 56,\n" +
            "        \"watchers\": 21183,\n" +
            "        \"default_branch\": \"master\"\n" +
            "      }\n" +
            "    },\n" +
            "    \"_links\": {\n" +
            "      \"self\": {\n" +
            "        \"href\": \"https://api.github.com/repos/square/retrofit/pulls/2336\"\n" +
            "      },\n" +
            "      \"html\": {\n" +
            "        \"href\": \"https://github.com/square/retrofit/pull/2336\"\n" +
            "      },\n" +
            "      \"issue\": {\n" +
            "        \"href\": \"https://api.github.com/repos/square/retrofit/issues/2336\"\n" +
            "      },\n" +
            "      \"comments\": {\n" +
            "        \"href\": \"https://api.github.com/repos/square/retrofit/issues/2336/comments\"\n" +
            "      },\n" +
            "      \"review_comments\": {\n" +
            "        \"href\": \"https://api.github.com/repos/square/retrofit/pulls/2336/comments\"\n" +
            "      },\n" +
            "      \"review_comment\": {\n" +
            "        \"href\": \"https://api.github.com/repos/square/retrofit/pulls/comments{/number}\"\n" +
            "      },\n" +
            "      \"commits\": {\n" +
            "        \"href\": \"https://api.github.com/repos/square/retrofit/pulls/2336/commits\"\n" +
            "      },\n" +
            "      \"statuses\": {\n" +
            "        \"href\": \"https://api.github.com/repos/square/retrofit/statuses/b542a834cba090a931a46746e7eb010ab5e71a6d\"\n" +
            "      }\n" +
            "    }\n" +
            "  },\n" +
            "  {\n" +
            "    \"url\": \"https://api.github.com/repos/square/retrofit/pulls/2330\",\n" +
            "    \"id\": 120435023,\n" +
            "    \"html_url\": \"https://github.com/square/retrofit/pull/2330\",\n" +
            "    \"diff_url\": \"https://github.com/square/retrofit/pull/2330.diff\",\n" +
            "    \"patch_url\": \"https://github.com/square/retrofit/pull/2330.patch\",\n" +
            "    \"issue_url\": \"https://api.github.com/repos/square/retrofit/issues/2330\",\n" +
            "    \"number\": 2330,\n" +
            "    \"state\": \"open\",\n" +
            "    \"locked\": false,\n" +
            "    \"title\": \"updating copyright notices\",\n" +
            "    \"user\": {\n" +
            "      \"login\": \"tristansokol\",\n" +
            "      \"id\": 867661,\n" +
            "      \"avatar_url\": \"https://avatars0.githubusercontent.com/u/867661?v=3\",\n" +
            "      \"gravatar_id\": \"\",\n" +
            "      \"url\": \"https://api.github.com/users/tristansokol\",\n" +
            "      \"html_url\": \"https://github.com/tristansokol\",\n" +
            "      \"followers_url\": \"https://api.github.com/users/tristansokol/followers\",\n" +
            "      \"following_url\": \"https://api.github.com/users/tristansokol/following{/other_user}\",\n" +
            "      \"gists_url\": \"https://api.github.com/users/tristansokol/gists{/gist_id}\",\n" +
            "      \"starred_url\": \"https://api.github.com/users/tristansokol/starred{/owner}{/repo}\",\n" +
            "      \"subscriptions_url\": \"https://api.github.com/users/tristansokol/subscriptions\",\n" +
            "      \"organizations_url\": \"https://api.github.com/users/tristansokol/orgs\",\n" +
            "      \"repos_url\": \"https://api.github.com/users/tristansokol/repos\",\n" +
            "      \"events_url\": \"https://api.github.com/users/tristansokol/events{/privacy}\",\n" +
            "      \"received_events_url\": \"https://api.github.com/users/tristansokol/received_events\",\n" +
            "      \"type\": \"User\",\n" +
            "      \"site_admin\": false\n" +
            "    },\n" +
            "    \"body\": \"I saw that the copyright on on the [html page](http://square.github.io/retrofit/) was from 2013, and that seemed wrong. Then I realized that there were a _ton_ of copyright notices that were from all over the place. \\r\\n\\r\\nI spoke with some friends at legal@ and it seems like a decent strategy around keeping the copyright dates accurate is to include both the original date, as well as the most recent date that the files were updated. So I wrote a bash script to do all that. \\r\\n\\r\\nI did some spot checking, and it should avoid things like external libraries pretty well and the like. Let me know what you think!   \",\n" +
            "    \"created_at\": \"2017-05-13T04:43:25Z\",\n" +
            "    \"updated_at\": \"2017-05-13T10:56:46Z\",\n" +
            "    \"closed_at\": null,\n" +
            "    \"merged_at\": null,\n" +
            "    \"merge_commit_sha\": \"3b232481b2a978eb3e11cb71040916acae1e3c6a\",\n" +
            "    \"assignee\": null,\n" +
            "    \"assignees\": [],\n" +
            "    \"requested_reviewers\": [\n" +
            "      {\n" +
            "        \"login\": \"JakeWharton\",\n" +
            "        \"id\": 66577,\n" +
            "        \"avatar_url\": \"https://avatars3.githubusercontent.com/u/66577?v=3\",\n" +
            "        \"gravatar_id\": \"\",\n" +
            "        \"url\": \"https://api.github.com/users/JakeWharton\",\n" +
            "        \"html_url\": \"https://github.com/JakeWharton\",\n" +
            "        \"followers_url\": \"https://api.github.com/users/JakeWharton/followers\",\n" +
            "        \"following_url\": \"https://api.github.com/users/JakeWharton/following{/other_user}\",\n" +
            "        \"gists_url\": \"https://api.github.com/users/JakeWharton/gists{/gist_id}\",\n" +
            "        \"starred_url\": \"https://api.github.com/users/JakeWharton/starred{/owner}{/repo}\",\n" +
            "        \"subscriptions_url\": \"https://api.github.com/users/JakeWharton/subscriptions\",\n" +
            "        \"organizations_url\": \"https://api.github.com/users/JakeWharton/orgs\",\n" +
            "        \"repos_url\": \"https://api.github.com/users/JakeWharton/repos\",\n" +
            "        \"events_url\": \"https://api.github.com/users/JakeWharton/events{/privacy}\",\n" +
            "        \"received_events_url\": \"https://api.github.com/users/JakeWharton/received_events\",\n" +
            "        \"type\": \"User\",\n" +
            "        \"site_admin\": false\n" +
            "      }\n" +
            "    ],\n" +
            "    \"milestone\": null,\n" +
            "    \"commits_url\": \"https://api.github.com/repos/square/retrofit/pulls/2330/commits\",\n" +
            "    \"review_comments_url\": \"https://api.github.com/repos/square/retrofit/pulls/2330/comments\",\n" +
            "    \"review_comment_url\": \"https://api.github.com/repos/square/retrofit/pulls/comments{/number}\",\n" +
            "    \"comments_url\": \"https://api.github.com/repos/square/retrofit/issues/2330/comments\",\n" +
            "    \"statuses_url\": \"https://api.github.com/repos/square/retrofit/statuses/9e56c9297908112a4434c6f90afad72a5c70baa7\",\n" +
            "    \"head\": {\n" +
            "      \"label\": \"tristansokol:updating-copyrights\",\n" +
            "      \"ref\": \"updating-copyrights\",\n" +
            "      \"sha\": \"9e56c9297908112a4434c6f90afad72a5c70baa7\",\n" +
            "      \"user\": {\n" +
            "        \"login\": \"tristansokol\",\n" +
            "        \"id\": 867661,\n" +
            "        \"avatar_url\": \"https://avatars0.githubusercontent.com/u/867661?v=3\",\n" +
            "        \"gravatar_id\": \"\",\n" +
            "        \"url\": \"https://api.github.com/users/tristansokol\",\n" +
            "        \"html_url\": \"https://github.com/tristansokol\",\n" +
            "        \"followers_url\": \"https://api.github.com/users/tristansokol/followers\",\n" +
            "        \"following_url\": \"https://api.github.com/users/tristansokol/following{/other_user}\",\n" +
            "        \"gists_url\": \"https://api.github.com/users/tristansokol/gists{/gist_id}\",\n" +
            "        \"starred_url\": \"https://api.github.com/users/tristansokol/starred{/owner}{/repo}\",\n" +
            "        \"subscriptions_url\": \"https://api.github.com/users/tristansokol/subscriptions\",\n" +
            "        \"organizations_url\": \"https://api.github.com/users/tristansokol/orgs\",\n" +
            "        \"repos_url\": \"https://api.github.com/users/tristansokol/repos\",\n" +
            "        \"events_url\": \"https://api.github.com/users/tristansokol/events{/privacy}\",\n" +
            "        \"received_events_url\": \"https://api.github.com/users/tristansokol/received_events\",\n" +
            "        \"type\": \"User\",\n" +
            "        \"site_admin\": false\n" +
            "      },\n" +
            "      \"repo\": {\n" +
            "        \"id\": 83990837,\n" +
            "        \"name\": \"retrofit\",\n" +
            "        \"full_name\": \"tristansokol/retrofit\",\n" +
            "        \"owner\": {\n" +
            "          \"login\": \"tristansokol\",\n" +
            "          \"id\": 867661,\n" +
            "          \"avatar_url\": \"https://avatars0.githubusercontent.com/u/867661?v=3\",\n" +
            "          \"gravatar_id\": \"\",\n" +
            "          \"url\": \"https://api.github.com/users/tristansokol\",\n" +
            "          \"html_url\": \"https://github.com/tristansokol\",\n" +
            "          \"followers_url\": \"https://api.github.com/users/tristansokol/followers\",\n" +
            "          \"following_url\": \"https://api.github.com/users/tristansokol/following{/other_user}\",\n" +
            "          \"gists_url\": \"https://api.github.com/users/tristansokol/gists{/gist_id}\",\n" +
            "          \"starred_url\": \"https://api.github.com/users/tristansokol/starred{/owner}{/repo}\",\n" +
            "          \"subscriptions_url\": \"https://api.github.com/users/tristansokol/subscriptions\",\n" +
            "          \"organizations_url\": \"https://api.github.com/users/tristansokol/orgs\",\n" +
            "          \"repos_url\": \"https://api.github.com/users/tristansokol/repos\",\n" +
            "          \"events_url\": \"https://api.github.com/users/tristansokol/events{/privacy}\",\n" +
            "          \"received_events_url\": \"https://api.github.com/users/tristansokol/received_events\",\n" +
            "          \"type\": \"User\",\n" +
            "          \"site_admin\": false\n" +
            "        },\n" +
            "        \"private\": false,\n" +
            "        \"html_url\": \"https://github.com/tristansokol/retrofit\",\n" +
            "        \"description\": \"Type-safe HTTP client for Android and Java by Square, Inc.\",\n" +
            "        \"fork\": true,\n" +
            "        \"url\": \"https://api.github.com/repos/tristansokol/retrofit\",\n" +
            "        \"forks_url\": \"https://api.github.com/repos/tristansokol/retrofit/forks\",\n" +
            "        \"keys_url\": \"https://api.github.com/repos/tristansokol/retrofit/keys{/key_id}\",\n" +
            "        \"collaborators_url\": \"https://api.github.com/repos/tristansokol/retrofit/collaborators{/collaborator}\",\n" +
            "        \"teams_url\": \"https://api.github.com/repos/tristansokol/retrofit/teams\",\n" +
            "        \"hooks_url\": \"https://api.github.com/repos/tristansokol/retrofit/hooks\",\n" +
            "        \"issue_events_url\": \"https://api.github.com/repos/tristansokol/retrofit/issues/events{/number}\",\n" +
            "        \"events_url\": \"https://api.github.com/repos/tristansokol/retrofit/events\",\n" +
            "        \"assignees_url\": \"https://api.github.com/repos/tristansokol/retrofit/assignees{/user}\",\n" +
            "        \"branches_url\": \"https://api.github.com/repos/tristansokol/retrofit/branches{/branch}\",\n" +
            "        \"tags_url\": \"https://api.github.com/repos/tristansokol/retrofit/tags\",\n" +
            "        \"blobs_url\": \"https://api.github.com/repos/tristansokol/retrofit/git/blobs{/sha}\",\n" +
            "        \"git_tags_url\": \"https://api.github.com/repos/tristansokol/retrofit/git/tags{/sha}\",\n" +
            "        \"git_refs_url\": \"https://api.github.com/repos/tristansokol/retrofit/git/refs{/sha}\",\n" +
            "        \"trees_url\": \"https://api.github.com/repos/tristansokol/retrofit/git/trees{/sha}\",\n" +
            "        \"statuses_url\": \"https://api.github.com/repos/tristansokol/retrofit/statuses/{sha}\",\n" +
            "        \"languages_url\": \"https://api.github.com/repos/tristansokol/retrofit/languages\",\n" +
            "        \"stargazers_url\": \"https://api.github.com/repos/tristansokol/retrofit/stargazers\",\n" +
            "        \"contributors_url\": \"https://api.github.com/repos/tristansokol/retrofit/contributors\",\n" +
            "        \"subscribers_url\": \"https://api.github.com/repos/tristansokol/retrofit/subscribers\",\n" +
            "        \"subscription_url\": \"https://api.github.com/repos/tristansokol/retrofit/subscription\",\n" +
            "        \"commits_url\": \"https://api.github.com/repos/tristansokol/retrofit/commits{/sha}\",\n" +
            "        \"git_commits_url\": \"https://api.github.com/repos/tristansokol/retrofit/git/commits{/sha}\",\n" +
            "        \"comments_url\": \"https://api.github.com/repos/tristansokol/retrofit/comments{/number}\",\n" +
            "        \"issue_comment_url\": \"https://api.github.com/repos/tristansokol/retrofit/issues/comments{/number}\",\n" +
            "        \"contents_url\": \"https://api.github.com/repos/tristansokol/retrofit/contents/{+path}\",\n" +
            "        \"compare_url\": \"https://api.github.com/repos/tristansokol/retrofit/compare/{base}...{head}\",\n" +
            "        \"merges_url\": \"https://api.github.com/repos/tristansokol/retrofit/merges\",\n" +
            "        \"archive_url\": \"https://api.github.com/repos/tristansokol/retrofit/{archive_format}{/ref}\",\n" +
            "        \"downloads_url\": \"https://api.github.com/repos/tristansokol/retrofit/downloads\",\n" +
            "        \"issues_url\": \"https://api.github.com/repos/tristansokol/retrofit/issues{/number}\",\n" +
            "        \"pulls_url\": \"https://api.github.com/repos/tristansokol/retrofit/pulls{/number}\",\n" +
            "        \"milestones_url\": \"https://api.github.com/repos/tristansokol/retrofit/milestones{/number}\",\n" +
            "        \"notifications_url\": \"https://api.github.com/repos/tristansokol/retrofit/notifications{?since,all,participating}\",\n" +
            "        \"labels_url\": \"https://api.github.com/repos/tristansokol/retrofit/labels{/name}\",\n" +
            "        \"releases_url\": \"https://api.github.com/repos/tristansokol/retrofit/releases{/id}\",\n" +
            "        \"deployments_url\": \"https://api.github.com/repos/tristansokol/retrofit/deployments\",\n" +
            "        \"created_at\": \"2017-03-05T18:02:45Z\",\n" +
            "        \"updated_at\": \"2017-03-05T18:02:49Z\",\n" +
            "        \"pushed_at\": \"2017-05-13T04:36:49Z\",\n" +
            "        \"git_url\": \"git://github.com/tristansokol/retrofit.git\",\n" +
            "        \"ssh_url\": \"git@github.com:tristansokol/retrofit.git\",\n" +
            "        \"clone_url\": \"https://github.com/tristansokol/retrofit.git\",\n" +
            "        \"svn_url\": \"https://github.com/tristansokol/retrofit\",\n" +
            "        \"homepage\": \"http://square.github.io/retrofit/\",\n" +
            "        \"size\": 4392,\n" +
            "        \"stargazers_count\": 0,\n" +
            "        \"watchers_count\": 0,\n" +
            "        \"language\": \"Java\",\n" +
            "        \"has_issues\": false,\n" +
            "        \"has_projects\": true,\n" +
            "        \"has_downloads\": true,\n" +
            "        \"has_wiki\": true,\n" +
            "        \"has_pages\": false,\n" +
            "        \"forks_count\": 0,\n" +
            "        \"mirror_url\": null,\n" +
            "        \"open_issues_count\": 0,\n" +
            "        \"forks\": 0,\n" +
            "        \"open_issues\": 0,\n" +
            "        \"watchers\": 0,\n" +
            "        \"default_branch\": \"master\"\n" +
            "      }\n" +
            "    },\n" +
            "    \"base\": {\n" +
            "      \"label\": \"square:master\",\n" +
            "      \"ref\": \"master\",\n" +
            "      \"sha\": \"a2ca2aa4fc49518ed93403cdae1dbad7a5045be1\",\n" +
            "      \"user\": {\n" +
            "        \"login\": \"square\",\n" +
            "        \"id\": 82592,\n" +
            "        \"avatar_url\": \"https://avatars3.githubusercontent.com/u/82592?v=3\",\n" +
            "        \"gravatar_id\": \"\",\n" +
            "        \"url\": \"https://api.github.com/users/square\",\n" +
            "        \"html_url\": \"https://github.com/square\",\n" +
            "        \"followers_url\": \"https://api.github.com/users/square/followers\",\n" +
            "        \"following_url\": \"https://api.github.com/users/square/following{/other_user}\",\n" +
            "        \"gists_url\": \"https://api.github.com/users/square/gists{/gist_id}\",\n" +
            "        \"starred_url\": \"https://api.github.com/users/square/starred{/owner}{/repo}\",\n" +
            "        \"subscriptions_url\": \"https://api.github.com/users/square/subscriptions\",\n" +
            "        \"organizations_url\": \"https://api.github.com/users/square/orgs\",\n" +
            "        \"repos_url\": \"https://api.github.com/users/square/repos\",\n" +
            "        \"events_url\": \"https://api.github.com/users/square/events{/privacy}\",\n" +
            "        \"received_events_url\": \"https://api.github.com/users/square/received_events\",\n" +
            "        \"type\": \"Organization\",\n" +
            "        \"site_admin\": false\n" +
            "      },\n" +
            "      \"repo\": {\n" +
            "        \"id\": 892275,\n" +
            "        \"name\": \"retrofit\",\n" +
            "        \"full_name\": \"square/retrofit\",\n" +
            "        \"owner\": {\n" +
            "          \"login\": \"square\",\n" +
            "          \"id\": 82592,\n" +
            "          \"avatar_url\": \"https://avatars3.githubusercontent.com/u/82592?v=3\",\n" +
            "          \"gravatar_id\": \"\",\n" +
            "          \"url\": \"https://api.github.com/users/square\",\n" +
            "          \"html_url\": \"https://github.com/square\",\n" +
            "          \"followers_url\": \"https://api.github.com/users/square/followers\",\n" +
            "          \"following_url\": \"https://api.github.com/users/square/following{/other_user}\",\n" +
            "          \"gists_url\": \"https://api.github.com/users/square/gists{/gist_id}\",\n" +
            "          \"starred_url\": \"https://api.github.com/users/square/starred{/owner}{/repo}\",\n" +
            "          \"subscriptions_url\": \"https://api.github.com/users/square/subscriptions\",\n" +
            "          \"organizations_url\": \"https://api.github.com/users/square/orgs\",\n" +
            "          \"repos_url\": \"https://api.github.com/users/square/repos\",\n" +
            "          \"events_url\": \"https://api.github.com/users/square/events{/privacy}\",\n" +
            "          \"received_events_url\": \"https://api.github.com/users/square/received_events\",\n" +
            "          \"type\": \"Organization\",\n" +
            "          \"site_admin\": false\n" +
            "        },\n" +
            "        \"private\": false,\n" +
            "        \"html_url\": \"https://github.com/square/retrofit\",\n" +
            "        \"description\": \"Type-safe HTTP client for Android and Java by Square, Inc.\",\n" +
            "        \"fork\": false,\n" +
            "        \"url\": \"https://api.github.com/repos/square/retrofit\",\n" +
            "        \"forks_url\": \"https://api.github.com/repos/square/retrofit/forks\",\n" +
            "        \"keys_url\": \"https://api.github.com/repos/square/retrofit/keys{/key_id}\",\n" +
            "        \"collaborators_url\": \"https://api.github.com/repos/square/retrofit/collaborators{/collaborator}\",\n" +
            "        \"teams_url\": \"https://api.github.com/repos/square/retrofit/teams\",\n" +
            "        \"hooks_url\": \"https://api.github.com/repos/square/retrofit/hooks\",\n" +
            "        \"issue_events_url\": \"https://api.github.com/repos/square/retrofit/issues/events{/number}\",\n" +
            "        \"events_url\": \"https://api.github.com/repos/square/retrofit/events\",\n" +
            "        \"assignees_url\": \"https://api.github.com/repos/square/retrofit/assignees{/user}\",\n" +
            "        \"branches_url\": \"https://api.github.com/repos/square/retrofit/branches{/branch}\",\n" +
            "        \"tags_url\": \"https://api.github.com/repos/square/retrofit/tags\",\n" +
            "        \"blobs_url\": \"https://api.github.com/repos/square/retrofit/git/blobs{/sha}\",\n" +
            "        \"git_tags_url\": \"https://api.github.com/repos/square/retrofit/git/tags{/sha}\",\n" +
            "        \"git_refs_url\": \"https://api.github.com/repos/square/retrofit/git/refs{/sha}\",\n" +
            "        \"trees_url\": \"https://api.github.com/repos/square/retrofit/git/trees{/sha}\",\n" +
            "        \"statuses_url\": \"https://api.github.com/repos/square/retrofit/statuses/{sha}\",\n" +
            "        \"languages_url\": \"https://api.github.com/repos/square/retrofit/languages\",\n" +
            "        \"stargazers_url\": \"https://api.github.com/repos/square/retrofit/stargazers\",\n" +
            "        \"contributors_url\": \"https://api.github.com/repos/square/retrofit/contributors\",\n" +
            "        \"subscribers_url\": \"https://api.github.com/repos/square/retrofit/subscribers\",\n" +
            "        \"subscription_url\": \"https://api.github.com/repos/square/retrofit/subscription\",\n" +
            "        \"commits_url\": \"https://api.github.com/repos/square/retrofit/commits{/sha}\",\n" +
            "        \"git_commits_url\": \"https://api.github.com/repos/square/retrofit/git/commits{/sha}\",\n" +
            "        \"comments_url\": \"https://api.github.com/repos/square/retrofit/comments{/number}\",\n" +
            "        \"issue_comment_url\": \"https://api.github.com/repos/square/retrofit/issues/comments{/number}\",\n" +
            "        \"contents_url\": \"https://api.github.com/repos/square/retrofit/contents/{+path}\",\n" +
            "        \"compare_url\": \"https://api.github.com/repos/square/retrofit/compare/{base}...{head}\",\n" +
            "        \"merges_url\": \"https://api.github.com/repos/square/retrofit/merges\",\n" +
            "        \"archive_url\": \"https://api.github.com/repos/square/retrofit/{archive_format}{/ref}\",\n" +
            "        \"downloads_url\": \"https://api.github.com/repos/square/retrofit/downloads\",\n" +
            "        \"issues_url\": \"https://api.github.com/repos/square/retrofit/issues{/number}\",\n" +
            "        \"pulls_url\": \"https://api.github.com/repos/square/retrofit/pulls{/number}\",\n" +
            "        \"milestones_url\": \"https://api.github.com/repos/square/retrofit/milestones{/number}\",\n" +
            "        \"notifications_url\": \"https://api.github.com/repos/square/retrofit/notifications{?since,all,participating}\",\n" +
            "        \"labels_url\": \"https://api.github.com/repos/square/retrofit/labels{/name}\",\n" +
            "        \"releases_url\": \"https://api.github.com/repos/square/retrofit/releases{/id}\",\n" +
            "        \"deployments_url\": \"https://api.github.com/repos/square/retrofit/deployments\",\n" +
            "        \"created_at\": \"2010-09-06T21:39:43Z\",\n" +
            "        \"updated_at\": \"2017-05-17T02:40:58Z\",\n" +
            "        \"pushed_at\": \"2017-05-17T02:36:31Z\",\n" +
            "        \"git_url\": \"git://github.com/square/retrofit.git\",\n" +
            "        \"ssh_url\": \"git@github.com:square/retrofit.git\",\n" +
            "        \"clone_url\": \"https://github.com/square/retrofit.git\",\n" +
            "        \"svn_url\": \"https://github.com/square/retrofit\",\n" +
            "        \"homepage\": \"http://square.github.io/retrofit/\",\n" +
            "        \"size\": 4435,\n" +
            "        \"stargazers_count\": 21183,\n" +
            "        \"watchers_count\": 21183,\n" +
            "        \"language\": \"Java\",\n" +
            "        \"has_issues\": true,\n" +
            "        \"has_projects\": true,\n" +
            "        \"has_downloads\": true,\n" +
            "        \"has_wiki\": true,\n" +
            "        \"has_pages\": true,\n" +
            "        \"forks_count\": 4368,\n" +
            "        \"mirror_url\": null,\n" +
            "        \"open_issues_count\": 56,\n" +
            "        \"forks\": 4368,\n" +
            "        \"open_issues\": 56,\n" +
            "        \"watchers\": 21183,\n" +
            "        \"default_branch\": \"master\"\n" +
            "      }\n" +
            "    },\n" +
            "    \"_links\": {\n" +
            "      \"self\": {\n" +
            "        \"href\": \"https://api.github.com/repos/square/retrofit/pulls/2330\"\n" +
            "      },\n" +
            "      \"html\": {\n" +
            "        \"href\": \"https://github.com/square/retrofit/pull/2330\"\n" +
            "      },\n" +
            "      \"issue\": {\n" +
            "        \"href\": \"https://api.github.com/repos/square/retrofit/issues/2330\"\n" +
            "      },\n" +
            "      \"comments\": {\n" +
            "        \"href\": \"https://api.github.com/repos/square/retrofit/issues/2330/comments\"\n" +
            "      },\n" +
            "      \"review_comments\": {\n" +
            "        \"href\": \"https://api.github.com/repos/square/retrofit/pulls/2330/comments\"\n" +
            "      },\n" +
            "      \"review_comment\": {\n" +
            "        \"href\": \"https://api.github.com/repos/square/retrofit/pulls/comments{/number}\"\n" +
            "      },\n" +
            "      \"commits\": {\n" +
            "        \"href\": \"https://api.github.com/repos/square/retrofit/pulls/2330/commits\"\n" +
            "      },\n" +
            "      \"statuses\": {\n" +
            "        \"href\": \"https://api.github.com/repos/square/retrofit/statuses/9e56c9297908112a4434c6f90afad72a5c70baa7\"\n" +
            "      }\n" +
            "    }\n" +
            "  },\n" +
            "  {\n" +
            "    \"url\": \"https://api.github.com/repos/square/retrofit/pulls/2310\",\n" +
            "    \"id\": 118776023,\n" +
            "    \"html_url\": \"https://github.com/square/retrofit/pull/2310\",\n" +
            "    \"diff_url\": \"https://github.com/square/retrofit/pull/2310.diff\",\n" +
            "    \"patch_url\": \"https://github.com/square/retrofit/pull/2310.patch\",\n" +
            "    \"issue_url\": \"https://api.github.com/repos/square/retrofit/issues/2310\",\n" +
            "    \"number\": 2310,\n" +
            "    \"state\": \"open\",\n" +
            "    \"locked\": false,\n" +
            "    \"title\": \"Add polymorphic converter example.\",\n" +
            "    \"user\": {\n" +
            "      \"login\": \"NightlyNexus\",\n" +
            "      \"id\": 4032667,\n" +
            "      \"avatar_url\": \"https://avatars1.githubusercontent.com/u/4032667?v=3\",\n" +
            "      \"gravatar_id\": \"\",\n" +
            "      \"url\": \"https://api.github.com/users/NightlyNexus\",\n" +
            "      \"html_url\": \"https://github.com/NightlyNexus\",\n" +
            "      \"followers_url\": \"https://api.github.com/users/NightlyNexus/followers\",\n" +
            "      \"following_url\": \"https://api.github.com/users/NightlyNexus/following{/other_user}\",\n" +
            "      \"gists_url\": \"https://api.github.com/users/NightlyNexus/gists{/gist_id}\",\n" +
            "      \"starred_url\": \"https://api.github.com/users/NightlyNexus/starred{/owner}{/repo}\",\n" +
            "      \"subscriptions_url\": \"https://api.github.com/users/NightlyNexus/subscriptions\",\n" +
            "      \"organizations_url\": \"https://api.github.com/users/NightlyNexus/orgs\",\n" +
            "      \"repos_url\": \"https://api.github.com/users/NightlyNexus/repos\",\n" +
            "      \"events_url\": \"https://api.github.com/users/NightlyNexus/events{/privacy}\",\n" +
            "      \"received_events_url\": \"https://api.github.com/users/NightlyNexus/received_events\",\n" +
            "      \"type\": \"User\",\n" +
            "      \"site_admin\": false\n" +
            "    },\n" +
            "    \"body\": \"A proposal for #2123.\",\n" +
            "    \"created_at\": \"2017-05-03T14:34:10Z\",\n" +
            "    \"updated_at\": \"2017-05-05T01:14:31Z\",\n" +
            "    \"closed_at\": null,\n" +
            "    \"merged_at\": null,\n" +
            "    \"merge_commit_sha\": \"e490a92f2ec9f7b907f5db8e55850ec741e25bd2\",\n" +
            "    \"assignee\": null,\n" +
            "    \"assignees\": [],\n" +
            "    \"requested_reviewers\": [],\n" +
            "    \"milestone\": null,\n" +
            "    \"commits_url\": \"https://api.github.com/repos/square/retrofit/pulls/2310/commits\",\n" +
            "    \"review_comments_url\": \"https://api.github.com/repos/square/retrofit/pulls/2310/comments\",\n" +
            "    \"review_comment_url\": \"https://api.github.com/repos/square/retrofit/pulls/comments{/number}\",\n" +
            "    \"comments_url\": \"https://api.github.com/repos/square/retrofit/issues/2310/comments\",\n" +
            "    \"statuses_url\": \"https://api.github.com/repos/square/retrofit/statuses/73aceedc6fa85d8789940a568b880f739e95b93c\",\n" +
            "    \"head\": {\n" +
            "      \"label\": \"NightlyNexus:eric/0503.poly\",\n" +
            "      \"ref\": \"eric/0503.poly\",\n" +
            "      \"sha\": \"73aceedc6fa85d8789940a568b880f739e95b93c\",\n" +
            "      \"user\": {\n" +
            "        \"login\": \"NightlyNexus\",\n" +
            "        \"id\": 4032667,\n" +
            "        \"avatar_url\": \"https://avatars1.githubusercontent.com/u/4032667?v=3\",\n" +
            "        \"gravatar_id\": \"\",\n" +
            "        \"url\": \"https://api.github.com/users/NightlyNexus\",\n" +
            "        \"html_url\": \"https://github.com/NightlyNexus\",\n" +
            "        \"followers_url\": \"https://api.github.com/users/NightlyNexus/followers\",\n" +
            "        \"following_url\": \"https://api.github.com/users/NightlyNexus/following{/other_user}\",\n" +
            "        \"gists_url\": \"https://api.github.com/users/NightlyNexus/gists{/gist_id}\",\n" +
            "        \"starred_url\": \"https://api.github.com/users/NightlyNexus/starred{/owner}{/repo}\",\n" +
            "        \"subscriptions_url\": \"https://api.github.com/users/NightlyNexus/subscriptions\",\n" +
            "        \"organizations_url\": \"https://api.github.com/users/NightlyNexus/orgs\",\n" +
            "        \"repos_url\": \"https://api.github.com/users/NightlyNexus/repos\",\n" +
            "        \"events_url\": \"https://api.github.com/users/NightlyNexus/events{/privacy}\",\n" +
            "        \"received_events_url\": \"https://api.github.com/users/NightlyNexus/received_events\",\n" +
            "        \"type\": \"User\",\n" +
            "        \"site_admin\": false\n" +
            "      },\n" +
            "      \"repo\": {\n" +
            "        \"id\": 35349496,\n" +
            "        \"name\": \"retrofit\",\n" +
            "        \"full_name\": \"NightlyNexus/retrofit\",\n" +
            "        \"owner\": {\n" +
            "          \"login\": \"NightlyNexus\",\n" +
            "          \"id\": 4032667,\n" +
            "          \"avatar_url\": \"https://avatars1.githubusercontent.com/u/4032667?v=3\",\n" +
            "          \"gravatar_id\": \"\",\n" +
            "          \"url\": \"https://api.github.com/users/NightlyNexus\",\n" +
            "          \"html_url\": \"https://github.com/NightlyNexus\",\n" +
            "          \"followers_url\": \"https://api.github.com/users/NightlyNexus/followers\",\n" +
            "          \"following_url\": \"https://api.github.com/users/NightlyNexus/following{/other_user}\",\n" +
            "          \"gists_url\": \"https://api.github.com/users/NightlyNexus/gists{/gist_id}\",\n" +
            "          \"starred_url\": \"https://api.github.com/users/NightlyNexus/starred{/owner}{/repo}\",\n" +
            "          \"subscriptions_url\": \"https://api.github.com/users/NightlyNexus/subscriptions\",\n" +
            "          \"organizations_url\": \"https://api.github.com/users/NightlyNexus/orgs\",\n" +
            "          \"repos_url\": \"https://api.github.com/users/NightlyNexus/repos\",\n" +
            "          \"events_url\": \"https://api.github.com/users/NightlyNexus/events{/privacy}\",\n" +
            "          \"received_events_url\": \"https://api.github.com/users/NightlyNexus/received_events\",\n" +
            "          \"type\": \"User\",\n" +
            "          \"site_admin\": false\n" +
            "        },\n" +
            "        \"private\": false,\n" +
            "        \"html_url\": \"https://github.com/NightlyNexus/retrofit\",\n" +
            "        \"description\": \"Type-safe REST client for Android and Java by Square, Inc.\",\n" +
            "        \"fork\": true,\n" +
            "        \"url\": \"https://api.github.com/repos/NightlyNexus/retrofit\",\n" +
            "        \"forks_url\": \"https://api.github.com/repos/NightlyNexus/retrofit/forks\",\n" +
            "        \"keys_url\": \"https://api.github.com/repos/NightlyNexus/retrofit/keys{/key_id}\",\n" +
            "        \"collaborators_url\": \"https://api.github.com/repos/NightlyNexus/retrofit/collaborators{/collaborator}\",\n" +
            "        \"teams_url\": \"https://api.github.com/repos/NightlyNexus/retrofit/teams\",\n" +
            "        \"hooks_url\": \"https://api.github.com/repos/NightlyNexus/retrofit/hooks\",\n" +
            "        \"issue_events_url\": \"https://api.github.com/repos/NightlyNexus/retrofit/issues/events{/number}\",\n" +
            "        \"events_url\": \"https://api.github.com/repos/NightlyNexus/retrofit/events\",\n" +
            "        \"assignees_url\": \"https://api.github.com/repos/NightlyNexus/retrofit/assignees{/user}\",\n" +
            "        \"branches_url\": \"https://api.github.com/repos/NightlyNexus/retrofit/branches{/branch}\",\n" +
            "        \"tags_url\": \"https://api.github.com/repos/NightlyNexus/retrofit/tags\",\n" +
            "        \"blobs_url\": \"https://api.github.com/repos/NightlyNexus/retrofit/git/blobs{/sha}\",\n" +
            "        \"git_tags_url\": \"https://api.github.com/repos/NightlyNexus/retrofit/git/tags{/sha}\",\n" +
            "        \"git_refs_url\": \"https://api.github.com/repos/NightlyNexus/retrofit/git/refs{/sha}\",\n" +
            "        \"trees_url\": \"https://api.github.com/repos/NightlyNexus/retrofit/git/trees{/sha}\",\n" +
            "        \"statuses_url\": \"https://api.github.com/repos/NightlyNexus/retrofit/statuses/{sha}\",\n" +
            "        \"languages_url\": \"https://api.github.com/repos/NightlyNexus/retrofit/languages\",\n" +
            "        \"stargazers_url\": \"https://api.github.com/repos/NightlyNexus/retrofit/stargazers\",\n" +
            "        \"contributors_url\": \"https://api.github.com/repos/NightlyNexus/retrofit/contributors\",\n" +
            "        \"subscribers_url\": \"https://api.github.com/repos/NightlyNexus/retrofit/subscribers\",\n" +
            "        \"subscription_url\": \"https://api.github.com/repos/NightlyNexus/retrofit/subscription\",\n" +
            "        \"commits_url\": \"https://api.github.com/repos/NightlyNexus/retrofit/commits{/sha}\",\n" +
            "        \"git_commits_url\": \"https://api.github.com/repos/NightlyNexus/retrofit/git/commits{/sha}\",\n" +
            "        \"comments_url\": \"https://api.github.com/repos/NightlyNexus/retrofit/comments{/number}\",\n" +
            "        \"issue_comment_url\": \"https://api.github.com/repos/NightlyNexus/retrofit/issues/comments{/number}\",\n" +
            "        \"contents_url\": \"https://api.github.com/repos/NightlyNexus/retrofit/contents/{+path}\",\n" +
            "        \"compare_url\": \"https://api.github.com/repos/NightlyNexus/retrofit/compare/{base}...{head}\",\n" +
            "        \"merges_url\": \"https://api.github.com/repos/NightlyNexus/retrofit/merges\",\n" +
            "        \"archive_url\": \"https://api.github.com/repos/NightlyNexus/retrofit/{archive_format}{/ref}\",\n" +
            "        \"downloads_url\": \"https://api.github.com/repos/NightlyNexus/retrofit/downloads\",\n" +
            "        \"issues_url\": \"https://api.github.com/repos/NightlyNexus/retrofit/issues{/number}\",\n" +
            "        \"pulls_url\": \"https://api.github.com/repos/NightlyNexus/retrofit/pulls{/number}\",\n" +
            "        \"milestones_url\": \"https://api.github.com/repos/NightlyNexus/retrofit/milestones{/number}\",\n" +
            "        \"notifications_url\": \"https://api.github.com/repos/NightlyNexus/retrofit/notifications{?since,all,participating}\",\n" +
            "        \"labels_url\": \"https://api.github.com/repos/NightlyNexus/retrofit/labels{/name}\",\n" +
            "        \"releases_url\": \"https://api.github.com/repos/NightlyNexus/retrofit/releases{/id}\",\n" +
            "        \"deployments_url\": \"https://api.github.com/repos/NightlyNexus/retrofit/deployments\",\n" +
            "        \"created_at\": \"2015-05-09T23:34:26Z\",\n" +
            "        \"updated_at\": \"2016-04-09T21:30:36Z\",\n" +
            "        \"pushed_at\": \"2017-05-05T01:14:30Z\",\n" +
            "        \"git_url\": \"git://github.com/NightlyNexus/retrofit.git\",\n" +
            "        \"ssh_url\": \"git@github.com:NightlyNexus/retrofit.git\",\n" +
            "        \"clone_url\": \"https://github.com/NightlyNexus/retrofit.git\",\n" +
            "        \"svn_url\": \"https://github.com/NightlyNexus/retrofit\",\n" +
            "        \"homepage\": \"http://square.github.io/retrofit/\",\n" +
            "        \"size\": 3894,\n" +
            "        \"stargazers_count\": 0,\n" +
            "        \"watchers_count\": 0,\n" +
            "        \"language\": \"Java\",\n" +
            "        \"has_issues\": false,\n" +
            "        \"has_projects\": true,\n" +
            "        \"has_downloads\": true,\n" +
            "        \"has_wiki\": false,\n" +
            "        \"has_pages\": false,\n" +
            "        \"forks_count\": 0,\n" +
            "        \"mirror_url\": null,\n" +
            "        \"open_issues_count\": 0,\n" +
            "        \"forks\": 0,\n" +
            "        \"open_issues\": 0,\n" +
            "        \"watchers\": 0,\n" +
            "        \"default_branch\": \"master\"\n" +
            "      }\n" +
            "    },\n" +
            "    \"base\": {\n" +
            "      \"label\": \"square:master\",\n" +
            "      \"ref\": \"master\",\n" +
            "      \"sha\": \"a63faf44e1b103a49d25a15c720fc5940ae9fc07\",\n" +
            "      \"user\": {\n" +
            "        \"login\": \"square\",\n" +
            "        \"id\": 82592,\n" +
            "        \"avatar_url\": \"https://avatars3.githubusercontent.com/u/82592?v=3\",\n" +
            "        \"gravatar_id\": \"\",\n" +
            "        \"url\": \"https://api.github.com/users/square\",\n" +
            "        \"html_url\": \"https://github.com/square\",\n" +
            "        \"followers_url\": \"https://api.github.com/users/square/followers\",\n" +
            "        \"following_url\": \"https://api.github.com/users/square/following{/other_user}\",\n" +
            "        \"gists_url\": \"https://api.github.com/users/square/gists{/gist_id}\",\n" +
            "        \"starred_url\": \"https://api.github.com/users/square/starred{/owner}{/repo}\",\n" +
            "        \"subscriptions_url\": \"https://api.github.com/users/square/subscriptions\",\n" +
            "        \"organizations_url\": \"https://api.github.com/users/square/orgs\",\n" +
            "        \"repos_url\": \"https://api.github.com/users/square/repos\",\n" +
            "        \"events_url\": \"https://api.github.com/users/square/events{/privacy}\",\n" +
            "        \"received_events_url\": \"https://api.github.com/users/square/received_events\",\n" +
            "        \"type\": \"Organization\",\n" +
            "        \"site_admin\": false\n" +
            "      },\n" +
            "      \"repo\": {\n" +
            "        \"id\": 892275,\n" +
            "        \"name\": \"retrofit\",\n" +
            "        \"full_name\": \"square/retrofit\",\n" +
            "        \"owner\": {\n" +
            "          \"login\": \"square\",\n" +
            "          \"id\": 82592,\n" +
            "          \"avatar_url\": \"https://avatars3.githubusercontent.com/u/82592?v=3\",\n" +
            "          \"gravatar_id\": \"\",\n" +
            "          \"url\": \"https://api.github.com/users/square\",\n" +
            "          \"html_url\": \"https://github.com/square\",\n" +
            "          \"followers_url\": \"https://api.github.com/users/square/followers\",\n" +
            "          \"following_url\": \"https://api.github.com/users/square/following{/other_user}\",\n" +
            "          \"gists_url\": \"https://api.github.com/users/square/gists{/gist_id}\",\n" +
            "          \"starred_url\": \"https://api.github.com/users/square/starred{/owner}{/repo}\",\n" +
            "          \"subscriptions_url\": \"https://api.github.com/users/square/subscriptions\",\n" +
            "          \"organizations_url\": \"https://api.github.com/users/square/orgs\",\n" +
            "          \"repos_url\": \"https://api.github.com/users/square/repos\",\n" +
            "          \"events_url\": \"https://api.github.com/users/square/events{/privacy}\",\n" +
            "          \"received_events_url\": \"https://api.github.com/users/square/received_events\",\n" +
            "          \"type\": \"Organization\",\n" +
            "          \"site_admin\": false\n" +
            "        },\n" +
            "        \"private\": false,\n" +
            "        \"html_url\": \"https://github.com/square/retrofit\",\n" +
            "        \"description\": \"Type-safe HTTP client for Android and Java by Square, Inc.\",\n" +
            "        \"fork\": false,\n" +
            "        \"url\": \"https://api.github.com/repos/square/retrofit\",\n" +
            "        \"forks_url\": \"https://api.github.com/repos/square/retrofit/forks\",\n" +
            "        \"keys_url\": \"https://api.github.com/repos/square/retrofit/keys{/key_id}\",\n" +
            "        \"collaborators_url\": \"https://api.github.com/repos/square/retrofit/collaborators{/collaborator}\",\n" +
            "        \"teams_url\": \"https://api.github.com/repos/square/retrofit/teams\",\n" +
            "        \"hooks_url\": \"https://api.github.com/repos/square/retrofit/hooks\",\n" +
            "        \"issue_events_url\": \"https://api.github.com/repos/square/retrofit/issues/events{/number}\",\n" +
            "        \"events_url\": \"https://api.github.com/repos/square/retrofit/events\",\n" +
            "        \"assignees_url\": \"https://api.github.com/repos/square/retrofit/assignees{/user}\",\n" +
            "        \"branches_url\": \"https://api.github.com/repos/square/retrofit/branches{/branch}\",\n" +
            "        \"tags_url\": \"https://api.github.com/repos/square/retrofit/tags\",\n" +
            "        \"blobs_url\": \"https://api.github.com/repos/square/retrofit/git/blobs{/sha}\",\n" +
            "        \"git_tags_url\": \"https://api.github.com/repos/square/retrofit/git/tags{/sha}\",\n" +
            "        \"git_refs_url\": \"https://api.github.com/repos/square/retrofit/git/refs{/sha}\",\n" +
            "        \"trees_url\": \"https://api.github.com/repos/square/retrofit/git/trees{/sha}\",\n" +
            "        \"statuses_url\": \"https://api.github.com/repos/square/retrofit/statuses/{sha}\",\n" +
            "        \"languages_url\": \"https://api.github.com/repos/square/retrofit/languages\",\n" +
            "        \"stargazers_url\": \"https://api.github.com/repos/square/retrofit/stargazers\",\n" +
            "        \"contributors_url\": \"https://api.github.com/repos/square/retrofit/contributors\",\n" +
            "        \"subscribers_url\": \"https://api.github.com/repos/square/retrofit/subscribers\",\n" +
            "        \"subscription_url\": \"https://api.github.com/repos/square/retrofit/subscription\",\n" +
            "        \"commits_url\": \"https://api.github.com/repos/square/retrofit/commits{/sha}\",\n" +
            "        \"git_commits_url\": \"https://api.github.com/repos/square/retrofit/git/commits{/sha}\",\n" +
            "        \"comments_url\": \"https://api.github.com/repos/square/retrofit/comments{/number}\",\n" +
            "        \"issue_comment_url\": \"https://api.github.com/repos/square/retrofit/issues/comments{/number}\",\n" +
            "        \"contents_url\": \"https://api.github.com/repos/square/retrofit/contents/{+path}\",\n" +
            "        \"compare_url\": \"https://api.github.com/repos/square/retrofit/compare/{base}...{head}\",\n" +
            "        \"merges_url\": \"https://api.github.com/repos/square/retrofit/merges\",\n" +
            "        \"archive_url\": \"https://api.github.com/repos/square/retrofit/{archive_format}{/ref}\",\n" +
            "        \"downloads_url\": \"https://api.github.com/repos/square/retrofit/downloads\",\n" +
            "        \"issues_url\": \"https://api.github.com/repos/square/retrofit/issues{/number}\",\n" +
            "        \"pulls_url\": \"https://api.github.com/repos/square/retrofit/pulls{/number}\",\n" +
            "        \"milestones_url\": \"https://api.github.com/repos/square/retrofit/milestones{/number}\",\n" +
            "        \"notifications_url\": \"https://api.github.com/repos/square/retrofit/notifications{?since,all,participating}\",\n" +
            "        \"labels_url\": \"https://api.github.com/repos/square/retrofit/labels{/name}\",\n" +
            "        \"releases_url\": \"https://api.github.com/repos/square/retrofit/releases{/id}\",\n" +
            "        \"deployments_url\": \"https://api.github.com/repos/square/retrofit/deployments\",\n" +
            "        \"created_at\": \"2010-09-06T21:39:43Z\",\n" +
            "        \"updated_at\": \"2017-05-17T02:40:58Z\",\n" +
            "        \"pushed_at\": \"2017-05-17T02:36:31Z\",\n" +
            "        \"git_url\": \"git://github.com/square/retrofit.git\",\n" +
            "        \"ssh_url\": \"git@github.com:square/retrofit.git\",\n" +
            "        \"clone_url\": \"https://github.com/square/retrofit.git\",\n" +
            "        \"svn_url\": \"https://github.com/square/retrofit\",\n" +
            "        \"homepage\": \"http://square.github.io/retrofit/\",\n" +
            "        \"size\": 4435,\n" +
            "        \"stargazers_count\": 21183,\n" +
            "        \"watchers_count\": 21183,\n" +
            "        \"language\": \"Java\",\n" +
            "        \"has_issues\": true,\n" +
            "        \"has_projects\": true,\n" +
            "        \"has_downloads\": true,\n" +
            "        \"has_wiki\": true,\n" +
            "        \"has_pages\": true,\n" +
            "        \"forks_count\": 4368,\n" +
            "        \"mirror_url\": null,\n" +
            "        \"open_issues_count\": 56,\n" +
            "        \"forks\": 4368,\n" +
            "        \"open_issues\": 56,\n" +
            "        \"watchers\": 21183,\n" +
            "        \"default_branch\": \"master\"\n" +
            "      }\n" +
            "    },\n" +
            "    \"_links\": {\n" +
            "      \"self\": {\n" +
            "        \"href\": \"https://api.github.com/repos/square/retrofit/pulls/2310\"\n" +
            "      },\n" +
            "      \"html\": {\n" +
            "        \"href\": \"https://github.com/square/retrofit/pull/2310\"\n" +
            "      },\n" +
            "      \"issue\": {\n" +
            "        \"href\": \"https://api.github.com/repos/square/retrofit/issues/2310\"\n" +
            "      },\n" +
            "      \"comments\": {\n" +
            "        \"href\": \"https://api.github.com/repos/square/retrofit/issues/2310/comments\"\n" +
            "      },\n" +
            "      \"review_comments\": {\n" +
            "        \"href\": \"https://api.github.com/repos/square/retrofit/pulls/2310/comments\"\n" +
            "      },\n" +
            "      \"review_comment\": {\n" +
            "        \"href\": \"https://api.github.com/repos/square/retrofit/pulls/comments{/number}\"\n" +
            "      },\n" +
            "      \"commits\": {\n" +
            "        \"href\": \"https://api.github.com/repos/square/retrofit/pulls/2310/commits\"\n" +
            "      },\n" +
            "      \"statuses\": {\n" +
            "        \"href\": \"https://api.github.com/repos/square/retrofit/statuses/73aceedc6fa85d8789940a568b880f739e95b93c\"\n" +
            "      }\n" +
            "    }\n" +
            "  }\n" +
            "]";
}
