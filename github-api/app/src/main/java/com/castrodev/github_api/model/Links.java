
package com.castrodev.github_api.model;

import com.google.gson.annotations.SerializedName;

import javax.annotation.Generated;

@Generated("net.hexar.json2pojo")
public class Links {

    @SerializedName("comments")
    private Comments mComments;
    @SerializedName("commits")
    private Commits mCommits;
    @SerializedName("html")
    private Html mHtml;
    @SerializedName("issue")
    private Issue mIssue;
    @SerializedName("review_comment")
    private ReviewComment mReviewComment;
    @SerializedName("review_comments")
    private ReviewComments mReviewComments;
    @SerializedName("self")
    private Self mSelf;
    @SerializedName("statuses")
    private Statuses mStatuses;

    public Comments getComments() {
        return mComments;
    }

    public void setComments(Comments comments) {
        mComments = comments;
    }

    public Commits getCommits() {
        return mCommits;
    }

    public void setCommits(Commits commits) {
        mCommits = commits;
    }

    public Html getHtml() {
        return mHtml;
    }

    public void setHtml(Html html) {
        mHtml = html;
    }

    public Issue getIssue() {
        return mIssue;
    }

    public void setIssue(Issue issue) {
        mIssue = issue;
    }

    public ReviewComment getReviewComment() {
        return mReviewComment;
    }

    public void setReviewComment(ReviewComment reviewComment) {
        mReviewComment = reviewComment;
    }

    public ReviewComments getReviewComments() {
        return mReviewComments;
    }

    public void setReviewComments(ReviewComments reviewComments) {
        mReviewComments = reviewComments;
    }

    public Self getSelf() {
        return mSelf;
    }

    public void setSelf(Self self) {
        mSelf = self;
    }

    public Statuses getStatuses() {
        return mStatuses;
    }

    public void setStatuses(Statuses statuses) {
        mStatuses = statuses;
    }

}
