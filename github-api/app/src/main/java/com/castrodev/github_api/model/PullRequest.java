
package com.castrodev.github_api.model;

import com.google.gson.annotations.SerializedName;

import java.util.Date;
import java.util.List;

import javax.annotation.Generated;

@Generated("net.hexar.json2pojo")
public class PullRequest {

    @SerializedName("assignee")
    private Object mAssignee;
    @SerializedName("assignees")
    private List<Object> mAssignees;
    @SerializedName("base")
    private Base mBase;
    @SerializedName("body")
    private String mBody;
    @SerializedName("closed_at")
    private Object mClosedAt;
    @SerializedName("comments_url")
    private String mCommentsUrl;
    @SerializedName("commits_url")
    private String mCommitsUrl;
    @SerializedName("created_at")
    private Date mCreatedAt;
    @SerializedName("diff_url")
    private String mDiffUrl;
    @SerializedName("head")
    private Head mHead;
    @SerializedName("html_url")
    private String mHtmlUrl;
    @SerializedName("id")
    private Long mId;
    @SerializedName("issue_url")
    private String mIssueUrl;
    @SerializedName("locked")
    private Boolean mLocked;
    @SerializedName("merge_commit_sha")
    private String mMergeCommitSha;
    @SerializedName("merged_at")
    private Object mMergedAt;
    @SerializedName("milestone")
    private Object mMilestone;
    @SerializedName("number")
    private Long mNumber;
    @SerializedName("patch_url")
    private String mPatchUrl;
    @SerializedName("requested_reviewers")
    private List<Object> mRequestedReviewers;
    @SerializedName("review_comment_url")
    private String mReviewCommentUrl;
    @SerializedName("review_comments_url")
    private String mReviewCommentsUrl;
    @SerializedName("state")
    private String mState;
    @SerializedName("statuses_url")
    private String mStatusesUrl;
    @SerializedName("title")
    private String mTitle;
    @SerializedName("updated_at")
    private String mUpdatedAt;
    @SerializedName("url")
    private String mUrl;
    @SerializedName("user")
    private User mUser;
    @SerializedName("_links")
    private Links mLinks;

    public Object getAssignee() {
        return mAssignee;
    }

    public void setAssignee(Object assignee) {
        mAssignee = assignee;
    }

    public List<Object> getAssignees() {
        return mAssignees;
    }

    public void setAssignees(List<Object> assignees) {
        mAssignees = assignees;
    }

    public Base getBase() {
        return mBase;
    }

    public void setBase(Base base) {
        mBase = base;
    }

    public String getBody() {
        return mBody;
    }

    public void setBody(String body) {
        mBody = body;
    }

    public Object getClosedAt() {
        return mClosedAt;
    }

    public void setClosedAt(Object closedAt) {
        mClosedAt = closedAt;
    }

    public String getCommentsUrl() {
        return mCommentsUrl;
    }

    public void setCommentsUrl(String commentsUrl) {
        mCommentsUrl = commentsUrl;
    }

    public String getCommitsUrl() {
        return mCommitsUrl;
    }

    public void setCommitsUrl(String commitsUrl) {
        mCommitsUrl = commitsUrl;
    }

    public Date getCreatedAt() {
        return mCreatedAt;
    }

    public void setCreatedAt(Date createdAt) {
        mCreatedAt = createdAt;
    }

    public String getDiffUrl() {
        return mDiffUrl;
    }

    public void setDiffUrl(String diffUrl) {
        mDiffUrl = diffUrl;
    }

    public Head getHead() {
        return mHead;
    }

    public void setHead(Head head) {
        mHead = head;
    }

    public String getHtmlUrl() {
        return mHtmlUrl;
    }

    public void setHtmlUrl(String htmlUrl) {
        mHtmlUrl = htmlUrl;
    }

    public Long getId() {
        return mId;
    }

    public void setId(Long id) {
        mId = id;
    }

    public String getIssueUrl() {
        return mIssueUrl;
    }

    public void setIssueUrl(String issueUrl) {
        mIssueUrl = issueUrl;
    }

    public Boolean getLocked() {
        return mLocked;
    }

    public void setLocked(Boolean locked) {
        mLocked = locked;
    }

    public String getMergeCommitSha() {
        return mMergeCommitSha;
    }

    public void setMergeCommitSha(String mergeCommitSha) {
        mMergeCommitSha = mergeCommitSha;
    }

    public Object getMergedAt() {
        return mMergedAt;
    }

    public void setMergedAt(Object mergedAt) {
        mMergedAt = mergedAt;
    }

    public Object getMilestone() {
        return mMilestone;
    }

    public void setMilestone(Object milestone) {
        mMilestone = milestone;
    }

    public Long getNumber() {
        return mNumber;
    }

    public void setNumber(Long number) {
        mNumber = number;
    }

    public String getPatchUrl() {
        return mPatchUrl;
    }

    public void setPatchUrl(String patchUrl) {
        mPatchUrl = patchUrl;
    }

    public List<Object> getRequestedReviewers() {
        return mRequestedReviewers;
    }

    public void setRequestedReviewers(List<Object> requestedReviewers) {
        mRequestedReviewers = requestedReviewers;
    }

    public String getReviewCommentUrl() {
        return mReviewCommentUrl;
    }

    public void setReviewCommentUrl(String reviewCommentUrl) {
        mReviewCommentUrl = reviewCommentUrl;
    }

    public String getReviewCommentsUrl() {
        return mReviewCommentsUrl;
    }

    public void setReviewCommentsUrl(String reviewCommentsUrl) {
        mReviewCommentsUrl = reviewCommentsUrl;
    }

    public String getState() {
        return mState;
    }

    public void setState(String state) {
        mState = state;
    }

    public String getStatusesUrl() {
        return mStatusesUrl;
    }

    public void setStatusesUrl(String statusesUrl) {
        mStatusesUrl = statusesUrl;
    }

    public String getTitle() {
        return mTitle;
    }

    public void setTitle(String title) {
        mTitle = title;
    }

    public String getUpdatedAt() {
        return mUpdatedAt;
    }

    public void setUpdatedAt(String updatedAt) {
        mUpdatedAt = updatedAt;
    }

    public String getUrl() {
        return mUrl;
    }

    public void setUrl(String url) {
        mUrl = url;
    }

    public User getUser() {
        return mUser;
    }

    public void setUser(User user) {
        mUser = user;
    }

    public Links get_links() {
        return mLinks;
    }

    public void set_links(Links Links) {
        mLinks = Links;
    }

}
