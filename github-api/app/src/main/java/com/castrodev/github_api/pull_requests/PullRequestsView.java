package com.castrodev.github_api.pull_requests;

import com.castrodev.github_api.model.PullRequest;

import java.util.List;

/**
 * Created by rodrigocastro on 17/05/17.
 */

public interface PullRequestsView {

    void showProgress();
    void hideProgress();
    void setItems(List<PullRequest> respository);
    void goToPullRequestsSite(PullRequest item);
    void showDefaultError();
    void showNetworkError();
    boolean isConnected();
}
