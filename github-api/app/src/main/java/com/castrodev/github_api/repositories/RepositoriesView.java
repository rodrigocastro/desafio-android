package com.castrodev.github_api.repositories;

import com.castrodev.github_api.model.GithubRepository;
import com.castrodev.github_api.model.Item;

/**
 * Created by rodrigocastro on 15/05/17.
 */

public interface RepositoriesView {

    void showProgress();
    void hideProgress();
    void setItems(GithubRepository respository);
    void goToPullRequests(Item item);
    void showDefaultError();
    void showNetworkError();
    boolean isConnected();
}
