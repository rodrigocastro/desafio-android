package com.castrodev.github_api.pull_requests;

import com.castrodev.github_api.model.PullRequest;

import java.util.List;

/**
 * Created by rodrigocastro on 17/05/17.
 */

class PullRequestsPresenterImpl implements PullRequestsPresenter, PullRequestsInteractor.OnFinishedListener {

    private final String userName, repositoryName;
    private PullRequestsView pullRequestView;
    private PullRequestsInteractor pullRequestsInteractor;

    public PullRequestsPresenterImpl(String userName, String repositoryName, PullRequestsInteractor pullRequestsInteractor) {
        this.pullRequestsInteractor = pullRequestsInteractor;
        this.userName = userName;
        this.repositoryName = repositoryName;
    }

    @Override
    public void bindView(PullRequestsView view) {
        if (view != null) {
            pullRequestView = view;
            pullRequestView.showProgress();

            if (!pullRequestView.isConnected()) {
                pullRequestView.showNetworkError();
                pullRequestView.hideProgress();
                return;
            }
        }

        pullRequestsInteractor.getPullRequests(userName, repositoryName, this);
    }

    @Override
    public void unbindView() {
        pullRequestView = null;
    }

    @Override
    public void reload() {
        if(pullRequestView!=null){
            pullRequestView.showProgress();
            pullRequestsInteractor.getPullRequests(userName, repositoryName, this);
        }
    }

    @Override
    public void onItemClicked(PullRequest item) {
        if (pullRequestView != null) {
            pullRequestView.goToPullRequestsSite(item);
        }
    }


    @Override
    public void onFinished(List<PullRequest> repository) {
        if (pullRequestView != null) {
            pullRequestView.setItems(repository);
            pullRequestView.hideProgress();
        }
    }

    @Override
    public void onDefaultError() {
        if (pullRequestView != null) {
            pullRequestView.showDefaultError();
            pullRequestView.hideProgress();
        }
    }

    public PullRequestsView getView() {
        return pullRequestView;
    }
}
