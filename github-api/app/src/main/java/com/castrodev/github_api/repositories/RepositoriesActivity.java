package com.castrodev.github_api.repositories;

import android.content.Context;
import android.content.Intent;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.DividerItemDecoration;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.Toolbar;
import android.view.View;
import android.widget.ProgressBar;
import android.widget.TextView;

import com.castrodev.github_api.R;
import com.castrodev.github_api.model.GithubRepository;
import com.castrodev.github_api.model.Item;
import com.castrodev.github_api.pull_requests.PullRequestsActivity;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;

/**
 * Created by rodrigocastro on 15/05/17.
 */

public class RepositoriesActivity extends AppCompatActivity implements RepositoriesView {

    public static final String REPOSITORY_NAME_KEY = "REPOSITORY_NAME_KEY";
    public static final String USER_NAME_KEY = "USER_NAME_KEY";

    @BindView(R.id.toolbar)
    Toolbar toolbar;
    @BindView(R.id.recycler_view)
    RecyclerView rvRepositories;
    @BindView(R.id.tv_error)
    TextView tvError;
    @BindView(R.id.progress)
    ProgressBar progressBar;
    @BindView(R.id.error_view)
    View errorView;

    private RepositoriesPresenterImpl presenter;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.list_activity);
        ButterKnife.bind(this);

        presenter = new RepositoriesPresenterImpl(new RepositoriesInteractorImpl());
        setupView();
    }

    private void setupView() {
        toolbar.setTitle(R.string.app_name);
        setSupportActionBar(toolbar);
    }

    @Override
    protected void onResume() {
        super.onResume();
        presenter.bindView(this);
    }

    @Override
    protected void onDestroy() {
        presenter.unbindView();
        super.onDestroy();
    }

    @Override
    public void showProgress() {
        handleProgressVisibility(true);
    }

    @Override
    public void hideProgress() {
        handleProgressVisibility(false);
    }

    private void handleProgressVisibility(boolean visible) {
        progressBar.setVisibility(visible ? View.VISIBLE : View.GONE);
        rvRepositories.setVisibility(visible ? View.GONE : View.VISIBLE);
    }

    @Override
    public void setItems(GithubRepository respository) {
        errorView.setVisibility(View.GONE);
        RepositoriesAdapter adapter = (RepositoriesAdapter) rvRepositories.getAdapter();
        if (adapter == null) {
            LinearLayoutManager layoutManager = new LinearLayoutManager(this);
            rvRepositories.setLayoutManager(layoutManager);
            DividerItemDecoration dividerItemDecoration = new DividerItemDecoration(rvRepositories.getContext(),
                    layoutManager.getOrientation());
            rvRepositories.addItemDecoration(dividerItemDecoration);
            rvRepositories.setAdapter(new RepositoriesAdapter(respository, presenter));
        } else {
            adapter.concatenateDataSet(respository);
        }
    }

    @Override
    public void goToPullRequests(Item repository) {
        Intent intent = new Intent(this, PullRequestsActivity.class);
        intent.putExtra(USER_NAME_KEY, repository.getOwner().getLogin());
        intent.putExtra(REPOSITORY_NAME_KEY, repository.getName());
        startActivity(intent);
    }

    @Override
    public void showDefaultError() {
        showErrorMessage(R.string.unexpected_error);
    }

    @Override
    public void showNetworkError() {
        showErrorMessage(R.string.internet_error);
    }

    private void showErrorMessage(int message) {
        rvRepositories.setVisibility(View.GONE);
        errorView.setVisibility(View.VISIBLE);
        tvError.setText(message);
    }

    @Override
    public boolean isConnected() {
        ConnectivityManager cm =
                (ConnectivityManager) getSystemService(Context.CONNECTIVITY_SERVICE);
        NetworkInfo netInfo = cm.getActiveNetworkInfo();
        return netInfo != null && netInfo.isConnectedOrConnecting();
    }

    @OnClick(R.id.bt_try_again)
    public void tryAgain(View v) {
        presenter.reload();
    }
}
